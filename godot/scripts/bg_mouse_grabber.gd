extends PanelContainer

var mouse_inside:bool = false

func mouse_in() -> void:
	mouse_inside = true

func mouse_out() -> void:
	mouse_inside = false

func vp_resized() -> void:
	rect_size = get_viewport().size

func _ready():
	connect("mouse_entered",self,"mouse_in")
	connect("mouse_exited",self,"mouse_out")
	get_viewport().connect("size_changed",self,"vp_resized")
	vp_resized()
