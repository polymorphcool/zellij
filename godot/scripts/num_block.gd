extends Container

signal value_changed

onready var glob = get_node( "/root/laces_globals" )

var mouse_num_in = false
var mouse_plus_in = false
var mouse_minus_in = false
var mouse_pressed = false
var mouse_pressed_pos = Vector2()

var disabled = false
var prev_value = 0
var value = 0
var value_min = 0
var value_max = null
var value_init = null
var value_step = 1
var sensitivity = 0.1
onready var value_type = glob.NUMBLOCK_TYPE_INT

var wait_before_start = 0.2
var wait_init = 0.1
var wait = wait_init

func _ready():
	
	#signals
# warning-ignore:return_value_discarded
	$num.connect( "mouse_entered", self, "num_in" )
# warning-ignore:return_value_discarded
	$num.connect( "mouse_exited", self, "num_out" )
	
# warning-ignore:return_value_discarded
	$plus.connect( "mouse_entered", self, "plus_in" )
# warning-ignore:return_value_discarded
	$plus.connect( "mouse_exited", self, "plus_out" )
# warning-ignore:return_value_discarded
	$plus.connect( "pressed", self, "plus_pressed" )
	
# warning-ignore:return_value_discarded
	$minus.connect( "mouse_entered", self, "minus_in" )
# warning-ignore:return_value_discarded
	$minus.connect( "mouse_exited", self, "minus_out" )
# warning-ignore:return_value_discarded
	$minus.connect( "pressed", self, "minus_pressed" )

func disable( b ):
	disabled = b
	$plus.disabled = b
	$minus.disabled = b
	if not b:
		$num.set( "custom_colors/font_color", null )
		$label.set( "custom_colors/font_color", null )
	else:
		var c = Color( 0.5,0.5,0.5,1.0 )
		$num.set( "custom_colors/font_color", c )
		$label.set( "custom_colors/font_color", c )
		mouse_pressed = false
		mouse_plus_in = false
		mouse_minus_in = false

func set_label( t ):
	$label.text = t

func set_value( v ):
	if v == null:
		return
	if value_init == null:
		value_init = v
	value = v
	$num.text = str( value )

func num_in():
	mouse_num_in = true
	mouse_plus_in = false
	mouse_minus_in = false

func num_out():
	mouse_num_in = false

func plus_in():
	mouse_num_in = false
	mouse_plus_in = true
	mouse_minus_in = false

func plus_out():
	mouse_plus_in = false

func minus_in():
	mouse_num_in = false
	mouse_plus_in = false
	mouse_minus_in = true

func minus_out():
	mouse_minus_in = false

func plus_pressed():
	value += value_step
	constraint_value()
	set_value( value )
	emit_signal( "value_changed", self, value )

func minus_pressed():
	value -= value_step
	constraint_value()
	set_value( value )
	emit_signal( "value_changed", self, value )

func _process(delta):
	if not disabled and mouse_pressed:
		wait -= delta
		if wait < 0:
			wait += wait_init
			if mouse_plus_in:
				plus_pressed()
			elif mouse_minus_in:
				minus_pressed()

func constraint_value():
	if value_min != null and value < value_min:
		value = value_min
	if value_max != null and value > value_max:
		value = value_max
	if value_type == glob.NUMBLOCK_TYPE_INT:
		value = int( value )

func _input(event):
	if disabled: 
		return
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT:
		if not event.doubleclick:
			mouse_pressed = event.pressed
			mouse_pressed_pos = event.position
			prev_value = value
			wait = wait_before_start
		else:
			mouse_pressed = false
			if mouse_num_in:
				set_value( value_init )
				emit_signal( "value_changed", self, value )
	elif mouse_num_in and mouse_pressed and event is InputEventMouseMotion:
		var diff = event.position - mouse_pressed_pos
		value = prev_value - diff.y * sensitivity
		constraint_value()
		set_value( value )
		emit_signal( "value_changed", self, value )
