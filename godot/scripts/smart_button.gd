extends BaseButton

signal smart_button_pressed
signal smart_button_multiple_pressed

var data = null
var last_click:float = 0
var click_count:int = 0

func smart_button_up() -> void:
	emit_signal( "smart_button_pressed", self )
	var now:int = OS.get_ticks_msec()
	if now-last_click > 500:
		click_count = 1
	else:
		click_count += 1
		emit_signal( "smart_button_multiple_pressed", self )
	last_click = now

func _ready():
# warning-ignore:return_value_discarded
	connect("button_up",self,"smart_button_up")
