extends Sprite

export (int, 5, 200) var edge = 10 setget set_edge
var last_vps = null
var is_ready = false

func set_edge( e ):
	if not is_ready:
		return
	edge = e
	last_vps = null

func resize():
	
	$grid_vp.size = Vector2( edge, edge )
	$grid_vp/h_line.clear_points()
	$grid_vp/h_line.add_point( Vector2( 0, 0.5 ) )
	$grid_vp/h_line.add_point( Vector2( edge, 0.5 ) )
	$grid_vp/v_line.clear_points()
	$grid_vp/v_line.add_point( Vector2( 0.5, 0 ) )
	$grid_vp/v_line.add_point( Vector2( 0.5, edge ) )
	
	var vps = get_viewport().size
	var sc = vps.x
	if sc < vps.y:
		sc = vps.y
	var offx = 1
	var offy = -1
	if int(sc) % 2 == 1:
		offx += edge * 0.5
		offy += edge * 0.5
	scale = Vector2( sc, sc )
	material.set_shader_param( "uv_size", Vector2( sc, sc ) )
	material.set_shader_param( "uv_offset", Vector2( offx, offy ) / edge )

func _ready():
	is_ready = true
	set_edge( edge )

func _process(delta):
	
	var vps = get_viewport().size
	if last_vps == null or last_vps != vps:
		last_vps = vps
		resize()
