extends Control

onready var zellij:Node = get_node("/root/zellij")

signal struct_op_changed
signal symetry_changed
signal node_op_changed
signal shape_op_add
signal shape_op_rm
signal shape_selected

onready var struct_panel:VBoxContainer = $panel/general/structure
onready var struct_edit:Button = $panel/general/structure/header/edit
onready var struct_fields:VBoxContainer = $panel/general/structure/fields

onready var shapes_panel:VBoxContainer = $panel/general/shapes
onready var shapes_edit:Button = $panel/general/shapes/header/edit
onready var shapes_fields:VBoxContainer = $panel/general/shapes/fields
onready var shapes_list:VBoxContainer = $panel/general/shapes/fields/list
onready var shape_btn:CheckBox = $panel/general/shapes/fields/shape_tmpl
onready var shape_node_ops:HBoxContainer = $panel/general/shapes/fields/node_operation

onready var tiling_panel:VBoxContainer = $panel/general/tiling
onready var tiling_edit:Button = $panel/general/tiling/header/edit
onready var tiling_fields:VBoxContainer = $panel/general/tiling/fields

onready var display_panel:VBoxContainer = $panel/general/display
onready var display_edit:Button = $panel/general/display/header/edit
onready var display_fields:VBoxContainer = $panel/general/display/fields

onready var export_panel:VBoxContainer = $panel/general/export
onready var export_edit:Button = $panel/general/export/header/edit
onready var export_fields:VBoxContainer = $panel/general/export/fields

onready var struct_ops:Array = [
	{ 
		'checkbox': $panel/general/structure/fields/operation/add, 
		'status': $panel/general/structure/fields/operation/add.pressed
	},
	{ 
		'checkbox': $panel/general/structure/fields/operation/rm, 
		'status': $panel/general/structure/fields/operation/rm.pressed
	}
]

onready var node_ops:Array = [
	{ 
		'checkbox': $panel/general/shapes/fields/node_operation/add, 
		'status': $panel/general/shapes/fields/node_operation/add.pressed
	},
	{ 
		'checkbox': $panel/general/shapes/fields/node_operation/rm, 
		'status': $panel/general/shapes/fields/node_operation/rm.pressed
	}
]

onready var sym_op:Array = [
	{ 
		'checkbox': $panel/general/symetry/axial_x, 
		'status': $panel/general/symetry/axial_x.pressed
	},
	{ 
		'checkbox': $panel/general/symetry/axial_y, 
		'status': $panel/general/symetry/axial_y.pressed
	},
	{ 
		'checkbox': $panel/general/symetry/axial_xy, 
		'status': $panel/general/symetry/axial_xy.pressed
	},
	{ 
		'checkbox': $panel/general/symetry/central, 
		'status': $panel/general/symetry/central.pressed
	}
]

var mouse_inside:bool = false

func panel_enable( i:int ) -> void:
	
	if i != 0:
		struct_edit.pressed = false
	if i != 1:
		shapes_edit.pressed = false
	if i != 2:
		tiling_edit.pressed = false
	if i != 3:
		display_edit.pressed = false
	if i != 4:
		export_edit.pressed = false
	
	struct_fields.visible = struct_edit.pressed
	shapes_fields.visible = shapes_edit.pressed
	tiling_fields.visible = tiling_edit.pressed
	display_fields.visible = display_edit.pressed
	export_fields.visible = export_edit.pressed
	
	$panel.rect_size = Vector2.ZERO

func struct_enable() -> void:
	for so in struct_ops:
		so.checkbox.pressed = false
		so.status = false
	panel_enable(0)

func shape_enable() -> void:
	panel_enable(1)

func tiling_enable() -> void:
	panel_enable(2)

func display_enable() -> void:
	panel_enable(3)

func export_enable() -> void:
	panel_enable(4)

func struct_op_radio() -> void:
	# who changed?
	var opi:int = -1
	var pressed_count:int = 0
	for i in range(0,struct_ops.size()):
		var op:Dictionary = struct_ops[i]
		if op.checkbox.pressed != op.status:
			opi = i
		if op.checkbox.pressed:
			pressed_count += 1
		op.status = op.checkbox.pressed
	if opi == -1 and pressed_count > 0:
		return
	if pressed_count == 0:
		emit_signal( "struct_op_changed", 0 )
	else:
		for i in range(0,struct_ops.size()):
			var op:Dictionary = struct_ops[i]
			if i != opi:
				op.checkbox.pressed = false
				op.status = op.checkbox.pressed
		if struct_ops[opi].checkbox.pressed:
			emit_signal( "struct_op_changed", opi+1 )
		else:
			emit_signal( "struct_op_changed", 0 )

func struct_sym_radio() -> void:
	# who changed?
	var symi:int = -1
	var pressed_count:int = 0
	for i in range(0,sym_op.size()):
		var ss:Dictionary = sym_op[i]
		if ss.checkbox.pressed != ss.status:
			symi = i
		if ss.checkbox.pressed:
			pressed_count += 1
		ss.status = ss.checkbox.pressed
	if symi == -1 and pressed_count > 0:
		return
	if pressed_count == 0:
		emit_signal( "symetry_changed", 0 )
	else:
		for i in range(0,sym_op.size()):
			var ss:Dictionary = sym_op[i]
			if i != symi:
				ss.checkbox.pressed = false
				ss.status = ss.checkbox.pressed
		if sym_op[symi].checkbox.pressed:
			emit_signal( "symetry_changed", symi+1 )
		else:
			emit_signal( "symetry_changed", 0 )

func struct_sym_select( sid:int ) -> void:
	for i in range(0,sym_op.size()):
		sym_op[i].checkbox.pressed = i == (sid)-1
		sym_op[i].status = sym_op[i].checkbox.pressed

func node_op_radio() -> void:
	# who changed?
	var opi:int = -1
	var pressed_count:int = 0
	for i in range(0,node_ops.size()):
		var op:Dictionary = node_ops[i]
		if op.checkbox.pressed != op.status:
			opi = i
		if op.checkbox.pressed:
			pressed_count += 1
		op.status = op.checkbox.pressed
	if opi == -1 and pressed_count > 0:
		return
	if pressed_count == 0:
		emit_signal( "node_op_changed", 0 )
	else:
		for i in range(0,node_ops.size()):
			var op:Dictionary = node_ops[i]
			if i != opi:
				op.checkbox.pressed = false
				op.status = op.checkbox.pressed
		if node_ops[opi].checkbox.pressed:
			emit_signal( "node_op_changed", opi+1 )
		else:
			emit_signal( "node_op_changed", 0 )

func node_op_select( nid:int ) -> void:
	for i in range(0,node_ops.size()):
		node_ops[i].checkbox.pressed = i == nid
	node_op_radio()

func mouse_in() -> void:
	mouse_inside = true

func mouse_out() -> void:
	mouse_inside = false

func connect_mouse( n:Node ) -> void:
	
	if n is Control:
# warning-ignore:return_value_discarded
		$n.connect("mouse_entered",self,"mouse_in")
# warning-ignore:return_value_discarded
		$n.connect("mouse_exited",self,"mouse_out")
	for c in n.get_children():
		connect_mouse(c)

func shape_op_pressed_add() -> void:
	emit_signal("shape_op_add")

func shape_op_pressed_rm() -> void:
	emit_signal("shape_op_rm")

func shape_select( i:int ) -> void:
	if i < 0:
		return
	var slist:VBoxContainer = $panel/general/shapes/fields/list
	var scount:int = slist.get_child_count()
	if i >= scount:
		return
	var btn:CheckBox = slist.get_child(i)
	btn.pressed = true
	shape_pressed( btn )

func shape_pressed( btn:CheckBox ) -> void:
	# locate the button
	var shapeid:int = -1
	for i in range(0, shapes_list.get_child_count()):
		var c:Button = shapes_list.get_child(i)
		if c == btn and c.pressed:
			shapeid = i
		elif c.pressed:
			c.pressed = false
	shape_node_ops.visible = shapeid != -1
	emit_signal( "shape_selected", shapeid )
	$panel.rect_size = Vector2.ZERO

func sync_shape(data:Array) -> void:
	var count:int = data.size()
	while shapes_list.get_child_count() < count:
		var cb:CheckBox = shape_btn.duplicate()
		cb.visible = true
		cb.pressed = false
# warning-ignore:return_value_discarded
		cb.connect("smart_button_pressed", self, "shape_pressed")
		shapes_list.add_child( cb )
	var display_node_ops:bool = false
	for i in range( 0, shapes_list.get_child_count() ):
		var cb:CheckBox = shapes_list.get_child(i)
		if i >= count:
			cb.visible = false
			continue
		if cb.pressed:
			display_node_ops = true
		cb.visible = true
		sync_shape_label( i, data[i] )
	shape_node_ops.visible = display_node_ops
	$panel.rect_size = Vector2.ZERO

func sync_shape_label( shape_id:int, shape:Dictionary ) -> void:
	if shape_id < 0 or shape_id >= shapes_list.get_child_count():
		return
	var cb:CheckBox = shapes_list.get_child(shape_id)
	var sid:String = str(shape_id)
	while sid.length() < 2:
		sid = '0' + sid
	cb.text = str(shape.nodes.size()) + ' nds / #'+sid

func _ready():
	
	struct_edit.pressed = false
	shapes_edit.pressed = false
	tiling_edit.pressed = false
	display_edit.pressed = false
# warning-ignore:return_value_discarded
	struct_edit.connect( "button_up", self, "struct_enable" )
# warning-ignore:return_value_discarded
	shapes_edit.connect( "button_up", self, "shape_enable" )
# warning-ignore:return_value_discarded
	tiling_edit.connect( "button_up", self, "tiling_enable" )
# warning-ignore:return_value_discarded
	display_edit.connect( "button_up", self, "display_enable" )
# warning-ignore:return_value_discarded
	export_edit.connect( "button_up", self, "export_enable" )
	struct_enable()
	shape_enable()
	tiling_enable()
	display_enable()
	export_enable()
	
	for so in struct_ops:
		so.checkbox.connect("button_up",self,"struct_op_radio")
	
	for ss in sym_op:
		ss.checkbox.connect("button_up",self,"struct_sym_radio")
	
# warning-ignore:return_value_discarded
	$panel/general/shapes/fields/operation/add.connect("button_up",self,"shape_op_pressed_add")
# warning-ignore:return_value_discarded
	$panel/general/shapes/fields/operation/rm.connect("button_up",self,"shape_op_pressed_rm")
	shape_node_ops.visible = false
	for no in node_ops:
		no.checkbox.connect("button_up",self,"node_op_radio")
	# enforce add
	node_op_select(0)
	
# warning-ignore:return_value_discarded
	$panel.connect("mouse_entered",self,"mouse_in")
# warning-ignore:return_value_discarded
	$panel.connect("mouse_exited",self,"mouse_out")
