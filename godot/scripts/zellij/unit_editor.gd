extends Node2D

onready var zellij:Node = get_node("/root/zellij")
onready var zellig_files:Node = get_node("/root/zellij_files")

onready var _file_name:LineEdit = $ui/panel/general/file_name
onready var _file_open:Button = $ui/panel/general/file_actions/open
onready var _file_save:Button = $ui/panel/general/file_actions/save
onready var _file_new:Button = $ui/panel/general/file_actions/new
onready var _file_duplicate:Button = $ui/panel/general/file_actions/duplicate
onready var _file_delete:Button = $ui/panel/general/file_actions/delete

onready var _struct_edit:Button = $ui/panel/general/structure/header/edit
onready var _struct_diagonals:Button = $ui/panel/general/structure/fields/diagonals
onready var _struct_subdiv:SpinBox = $ui/panel/general/structure/fields/subdiv
onready var _struct_gen:Button = $ui/panel/general/structure/fields/generate

onready var _shapes_edit:Button = $ui/panel/general/shapes/header/edit

onready var _display_size_sp:SpinBox = $ui/panel/general/display/fields/size
onready var _display_info_cb:Button = $ui/panel/general/display/fields/info

onready var operation:int = zellij.OPS.NONE
onready var struct_symetry:int = zellij.SYMETRY.NONE

var nodes_picked:Array = []
var segments_picked:Array = []
var corner_symmetrics:Array = []
var new_unit:bool = true
var shape_id:int = -1

func file_open() -> void:
	$ui/panel/general/files.visible = true
	$ui/panel/general/files.enabled = true

func file_save() -> void:
	if $unit.resource_path == '':
		$unit.resource_path = zellig_files.get_valid_path( $unit.resource_name )
	zellig_files.serialise_unit( $unit.serialise(), $unit.resource_path )

func file_new() -> void:
	$unit.resource_path = ''
	$unit.resource_name = ''
	file_check()
	struct_generate()

func file_duplicate() -> void:
	pass

func file_delete() -> void:
	pass

func file_name( s:String ) -> void:
	$unit.resource_name = s

func file_check() -> void:
	_file_duplicate.disabled = $unit.resource_path == ''
	_file_delete.disabled = $unit.resource_path == ''
	if $unit.resource_name == '':
		$unit.resource_name = "zellij_u" + str( OS.get_unix_time() )
	_file_name.text = $unit.resource_name

func reset_actions() -> void:
	operation = zellij.OPS.NONE
	nodes_picked = []

func enable_new_unit() -> void:
	if new_unit:
		new_unit = false
		_file_new.disabled = false

func struct_enable() -> void:
	$unit.nodes = false
	$unit.highlight_shape( -1 )
	$ui/panel/general/files.visible = false
	reset_actions()
	if _struct_edit.pressed:
		$ui.struct_sym_select( struct_symetry )
	if not _struct_edit.pressed and _shapes_edit.pressed:
		shape_enable()

func struct_generate() -> void:
	$unit.diagonals = _struct_diagonals.pressed
	$unit.subdivision = _struct_subdiv.value
	$unit.generate = true
	$ui.sync_shape( $unit.get_shapes() )
	$ui/panel/general/files.visible = false
	enable_new_unit()

func display_size_sp( f:float ) -> void:
	$unit.display_size = f
	$unit.redraw()
	$unit.highlight_shape( shape_id )

func info_enable() -> void:
	$unit.labels = _display_info_cb.pressed

func shape_enable() -> void:
	reset_actions()
	struct_op_changed(0)
	shape_selected(shape_id)
	$ui/panel/general/files.visible = false
	if not _shapes_edit.pressed and _struct_edit.pressed:
		struct_enable()

func struct_op_changed( i:int ) -> void:
	nodes_picked = []
	segments_picked = []
	for l in $lines.get_children():
		l.visible = false
	match i:
		0:
			operation = zellij.OPS.NONE
			$unit.nodes = false
			$unit.pick_segment = false
		1:
			operation = zellij.OPS.STRUCT_ADD
			$unit.nodes = true
			$unit.pick_segment = false
		2:
			operation = zellij.OPS.STRUCT_REMOVE
			$unit.nodes = false
			$unit.pick_segment = true

func node_op_changed( i:int ) -> void:
	match i:
		0:
			#enforce add
			$ui.node_op_select(0)
		1:
			operation = zellij.OPS.SHAPE_NODE_ADD
		2:
			operation = zellij.OPS.SHAPE_NODE_REMOVE
			if shape_id != -1 and $unit.get_shape( shape_id ).size() == 0:
				#enforce add
				$ui.node_op_select(0)

func symetry_changed( i:int ) -> void:
	match operation:
		zellij.OPS.SHAPE_REMOVE:
			$ui.struct_sym_select( zellij.SYMETRY.NONE )
		zellij.OPS.SHAPE_NODE_ADD:
			if shape_id != -1:
				$unit.set_shape_symetry( shape_id, i )
			else:
				$ui.struct_sym_select( zellij.SYMETRY.NONE )
		zellij.OPS.SHAPE_NODE_REMOVE:
			if shape_id != -1:
				$unit.set_shape_symetry( shape_id, i )
			else:
				$ui.struct_sym_select( zellij.SYMETRY.NONE )
		zellij.OPS.STRUCT_REMOVE:
			struct_symetry = i
		zellij.OPS.STRUCT_ADD:
			struct_symetry = i
			match nodes_picked.size():
				1:
					get_symmetric_nodes( nodes_picked[0].data.pos )
		_:
			struct_symetry = i

func get_symmetric_nodes( pos:Vector2 ) -> void:
	corner_symmetrics = []
	var srci:int = $unit.node_exists( pos )
	if srci == -1:
		return
	var i:int = -1
	var ids:Array = []
	match struct_symetry:
		zellij.SYMETRY.AXIAL_X:
			i = $unit.node_exists( pos * Vector2(-1,1) )
			if i != -1:
				corner_symmetrics.append( $unit.get_node_at(i) )
		zellij.SYMETRY.AXIAL_Y:
			i = $unit.node_exists( pos * Vector2(1,-1) )
			if i != -1:
				corner_symmetrics.append( $unit.get_node_at(i) )
		zellij.SYMETRY.AXIAL_XY:
			i = $unit.node_exists( pos * Vector2(-1,1) )
			if i != -1:
				corner_symmetrics.append( $unit.get_node_at(i) )
				ids.append(i)
			else:
				corner_symmetrics.append( null )
			i = $unit.node_exists( pos * Vector2(-1,-1) )
			if i != -1:
				corner_symmetrics.append( $unit.get_node_at(i) )
				ids.append(i)
			else:
				corner_symmetrics.append( null )
			i = $unit.node_exists( pos * Vector2(1,-1) )
			if i != -1:
				corner_symmetrics.append( $unit.get_node_at(i) )
			else:
				corner_symmetrics.append( null )
		zellij.SYMETRY.CENTRAL:
			i = $unit.node_exists( pos * Vector2(-1,-1) )
			if i != -1:
				corner_symmetrics.append( $unit.get_node_at(i) )

func node_changed( btn:TextureButton ) -> void:
	
	match operation:
		zellij.OPS.SHAPE_NODE_ADD:
			$unit.add_shape_node( shape_id, btn.data.id )
			$unit.highlight_shape( shape_id )
			$ui.sync_shape_label( shape_id, $unit.get_shape( shape_id ) )
		zellij.OPS.SHAPE_NODE_REMOVE:
			$unit.remove_shape_node( shape_id, btn.data.id )
			$unit.highlight_shape( shape_id )
			$ui.sync_shape_label( shape_id, $unit.get_shape( shape_id ) )
		zellij.OPS.STRUCT_ADD:
			if not btn in nodes_picked:
				nodes_picked.append( btn )
			match nodes_picked.size():
				1:
					get_symmetric_nodes( nodes_picked[0].data.pos )
				2:
					var new_segments:Array = []
					new_segments.append([ nodes_picked[0].data.id, nodes_picked[1].data.id ])
					# storing extra segmments depending on symmetry
					var mult:Vector2
					match struct_symetry:
						zellij.SYMETRY.AXIAL_X:
							mult = Vector2(-1,1)
							new_segments.append([
								$unit.node_exists( nodes_picked[0].data.pos * mult ), 
								$unit.node_exists( nodes_picked[1].data.pos * mult )
								])
						zellij.SYMETRY.AXIAL_Y:
							mult = Vector2(1,-1)
							new_segments.append([
								$unit.node_exists( nodes_picked[0].data.pos * mult ), 
								$unit.node_exists( nodes_picked[1].data.pos * mult )
								])
						zellij.SYMETRY.AXIAL_XY:
							mult = Vector2(-1,1)
							new_segments.append([
								$unit.node_exists( nodes_picked[0].data.pos * mult ), 
								$unit.node_exists( nodes_picked[1].data.pos * mult )
								])
							mult = Vector2(-1,-1)
							new_segments.append([
								$unit.node_exists( nodes_picked[0].data.pos * mult ), 
								$unit.node_exists( nodes_picked[1].data.pos * mult )
								])
							mult = Vector2(1,-1)
							new_segments.append([
								$unit.node_exists( nodes_picked[0].data.pos * mult ), 
								$unit.node_exists( nodes_picked[1].data.pos * mult )
								])
						zellij.SYMETRY.CENTRAL:
							mult = Vector2(-1,-1)
							new_segments.append([
								$unit.node_exists( nodes_picked[0].data.pos * mult ), 
								$unit.node_exists( nodes_picked[1].data.pos * mult )
								])
					$unit.create_segments( new_segments )
					reset_nodes()
					enable_new_unit()

func reset_nodes() -> void:
	nodes_picked = []
	$unit.reset_nodes()
	for l in $lines.get_children():
		l.visible = false

func segment_changed() -> void:
	
	if $unit.segment_picked == -1:
		return
	
	segments_picked = [ $unit.segment_picked ]
	var curr_seg:Dictionary = $unit.get_segment_at( $unit.segment_picked )
	if curr_seg.empty():
		return
	
	match operation:
		zellij.OPS.STRUCT_REMOVE:
			var mult:Vector2
			var oi:int
			match struct_symetry:
				zellij.SYMETRY.AXIAL_X:
					mult = Vector2(-1,1)
					oi = $unit.segment_exists( $unit.node_exists( curr_seg.pos0 * mult ),$unit.node_exists( curr_seg.pos1 * mult ))
					if oi != -1:
						if not oi in segments_picked:
							segments_picked.append(oi)
						$unit.highlight_segment(oi)
				zellij.SYMETRY.AXIAL_Y:
					mult = Vector2(1,-1)
					oi = $unit.segment_exists( $unit.node_exists( curr_seg.pos0 * mult ),$unit.node_exists( curr_seg.pos1 * mult ))
					if oi != -1:
						if not oi in segments_picked:
							segments_picked.append(oi)
						$unit.highlight_segment(oi)
				zellij.SYMETRY.AXIAL_XY:
					mult = Vector2(-1,1)
					oi = $unit.segment_exists( $unit.node_exists( curr_seg.pos0 * mult ),$unit.node_exists( curr_seg.pos1 * mult ))
					if oi != -1:
						if not oi in segments_picked:
							segments_picked.append(oi)
						$unit.highlight_segment(oi)
					mult = Vector2(-1,-1)
					oi = $unit.segment_exists( $unit.node_exists( curr_seg.pos0 * mult ),$unit.node_exists( curr_seg.pos1 * mult ))
					if oi != -1:
						if not oi in segments_picked:
							segments_picked.append(oi)
						$unit.highlight_segment(oi)
					mult = Vector2(1,-1)
					oi = $unit.segment_exists( $unit.node_exists( curr_seg.pos0 * mult ),$unit.node_exists( curr_seg.pos1 * mult ))
					if oi != -1:
						if not oi in segments_picked:
							segments_picked.append(oi)
						$unit.highlight_segment(oi)
				zellij.SYMETRY.CENTRAL:
					mult = Vector2(-1,-1)
					oi = $unit.segment_exists( $unit.node_exists( curr_seg.pos0 * mult ),$unit.node_exists( curr_seg.pos1 * mult ))
					if oi != -1:
						if not oi in segments_picked:
							segments_picked.append(oi)
						$unit.highlight_segment(oi)

func shape_op_add() -> void:
	var slist:Array = $unit.add_shape()
	$ui.sync_shape( slist )
	$ui.shape_select( slist.size()-1 )

func shape_op_rm() -> void:
	operation = zellij.OPS.SHAPE_REMOVE
	if shape_id == -1:
		return
	$unit.remove_shape( shape_id )
	var slist:Array = $unit.get_shapes()
	$ui.sync_shape( slist )
# warning-ignore:narrowing_conversion
	shape_id = min( slist.size() - 1, shape_id )
	if shape_id != -1:
		$ui.shape_select( shape_id )
	else:
		shape_selected( -1 )

func shape_selected( i:int ) -> void:
	
	shape_id = i
	reset_nodes()
	
	if shape_id == -1 and operation == zellij.OPS.SHAPE_NODE_ADD:
		operation = zellij.OPS.NONE
	elif shape_id != -1 and operation != zellij.OPS.SHAPE_NODE_ADD:
		operation = zellij.OPS.SHAPE_NODE_ADD
	
	$unit.nodes = operation == zellij.OPS.SHAPE_NODE_ADD
	$unit.highlight_shape( shape_id )
	
	var curr_shape:Dictionary = $unit.get_shape( shape_id )
	if not curr_shape.empty():
		$ui.struct_sym_select( curr_shape.symetry )
		if $unit.get_shape( shape_id ).size() == 0:
			# enforce add
			$ui.node_op_select(0)
	else:
		$ui.struct_sym_select( zellij.SYMETRY.NONE )

func _ready():
	
	$unit.nodes = false
	$unit.segments = true
	$unit.labels = false
	$unit.pick_segment = false
# warning-ignore:return_value_discarded
	$unit.connect("node_changed",self,"node_changed")
# warning-ignore:return_value_discarded
	$unit.connect("segment_changed",self,"segment_changed")
	
# warning-ignore:return_value_discarded
	_file_open.connect("button_up",self,"file_open")
# warning-ignore:return_value_discarded
	_file_save.connect("button_up",self,"file_save")
# warning-ignore:return_value_discarded
	_file_new.connect("button_up",self,"file_new")
# warning-ignore:return_value_discarded
	_file_duplicate.connect("button_up",self,"file_duplicate")
# warning-ignore:return_value_discarded
	_file_delete.connect("button_up",self,"file_delete")
# warning-ignore:return_value_discarded
	_file_name.connect("text_changed",self,"file_name")
	file_check()
	
# warning-ignore:return_value_discarded
	_struct_edit.connect("button_up",self,"struct_enable")
	_struct_diagonals.pressed = $unit.diagonals
	_struct_subdiv.value = $unit.subdivision
# warning-ignore:return_value_discarded
	_struct_gen.connect("button_up",self,"struct_generate")
	_display_size_sp.value = $unit.display_size
# warning-ignore:return_value_discarded
	_display_size_sp.connect("value_changed", self, "display_size_sp")
# warning-ignore:return_value_discarded
	_display_info_cb.connect("button_up",self,"info_enable")
# warning-ignore:return_value_discarded
	_shapes_edit.connect("button_up",self,"shape_enable")
	
# warning-ignore:return_value_discarded
	$ui.connect("struct_op_changed",self,"struct_op_changed")
# warning-ignore:return_value_discarded
	$ui.connect("symetry_changed",self,"symetry_changed")
# warning-ignore:return_value_discarded
	$ui.connect("node_op_changed",self,"node_op_changed")
# warning-ignore:return_value_discarded
	$ui.connect("shape_op_add",self,"shape_op_add")
# warning-ignore:return_value_discarded
	$ui.connect("shape_op_rm",self,"shape_op_rm")
# warning-ignore:return_value_discarded
	$ui.connect("shape_selected",self,"shape_selected")

func _input(event):
	
	if event is InputEventMouseButton and event.pressed:
		
		match operation:
			zellij.OPS.STRUCT_REMOVE:
				if $ui.mouse_inside:
					return
				if not segments_picked.empty() and $unit.segment_picked != -1:
					$unit.remove_segments( segments_picked )
					segments_picked = []
					enable_new_unit()
			zellij.OPS.STRUCT_ADD:
				if not nodes_picked.empty() and $bg.mouse_inside:
					reset_nodes()

func _process(delta) -> void:
	
	match operation:
		zellij.OPS.STRUCT_ADD:
			if not nodes_picked.empty():
				var l:Line2D
				var mp:Vector2 = get_viewport().get_mouse_position()
				var pt:Vector2 = $unit.position + nodes_picked[0].data.pos * $unit.display_size
				l = $lines.get_child(0)
				l.visible = true
				l.clear_points()
				l.add_point( pt )
				l.add_point( mp )
				var rel:Vector2 = mp - pt
				for i in range(1,4):
					l = $lines.get_child(i)
					var ci:int = i-1
					if ci >= corner_symmetrics.size() or corner_symmetrics[ci] == null:
						l.visible = false
						continue
					l.visible = true
					l.clear_points()
					match struct_symetry:
						zellij.SYMETRY.AXIAL_X:
							l.add_point( $unit.position + corner_symmetrics[ci] * $unit.display_size )
							l.add_point( $unit.position + corner_symmetrics[ci] * $unit.display_size + rel * Vector2(-1,1) )
						zellij.SYMETRY.AXIAL_Y:
							l.add_point( $unit.position + corner_symmetrics[ci] * $unit.display_size )
							l.add_point( $unit.position + corner_symmetrics[ci] * $unit.display_size + rel * Vector2(1,-1) )
						zellij.SYMETRY.AXIAL_XY:
							l.add_point( $unit.position + corner_symmetrics[ci] * $unit.display_size )
							var mult:Vector2 = Vector2(-1,1)
							match ci:
								1:
									mult = Vector2(-1,-1)
								2:
									mult = Vector2(1,-1)
							l.add_point( $unit.position + corner_symmetrics[ci] * $unit.display_size + rel * mult )
						zellij.SYMETRY.CENTRAL:
							l.add_point( $unit.position + corner_symmetrics[ci] * $unit.display_size )
							l.add_point( $unit.position + corner_symmetrics[ci] * $unit.display_size + rel * Vector2(-1,-1) )
