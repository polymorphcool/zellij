extends Node

onready var glob = get_node( "/root/laces_globals" )

func load_configurations():
	
	var file = File.new()
	var err = file.open( glob.LACE_DATAPATH, File.READ )
	if err == OK:
		var data = parse_json(file.get_as_text())
		for cfg in data:
			glob.check_configuration( cfg )
		file.close()
		return data
	else:
		return null

func save_configurations( ls ):
	
	var file = File.new()
	var err = file.open( glob.LACE_DATAPATH, File.WRITE )
	if err == OK:
		file.store_string(to_json(ls))
		file.close()
