extends Node

const tmp_folder:String = "user://tmp/"
const default_save_folder:String = "user://units/"

const tmp_svg_edit:String = tmp_folder + 'zellij_edit.svg'
const tmp_svg_tile:String = tmp_folder + 'zellij_tile.svg'

const svg_tmp_fill:Color = Color(1,1,1,0.85)

enum OPS {
	NONE = 					0,
	STRUCT_ADD = 			1,
	STRUCT_REMOVE = 		2,
	SHAPE_REMOVE = 			3,
	SHAPE_NODE_ADD = 		4,
	SHAPE_NODE_REMOVE = 	5
}

enum SYMETRY {
	NONE = 					0,
	AXIAL_X = 				1,
	AXIAL_Y = 				2,
	AXIAL_XY = 				3,
	CENTRAL = 				4
}

enum EDGE_SORT {
	DEFAULT = 				0, 	# in creation order
	INVERSE = 				1, 	# inverse to creation
	SMALL = 				2, 	# small first
	BIG = 					3 	# big first
}

enum CLONE_SORT {
	DEFAULT = 				0, 	# by rotation
	ALIGN = 				1,
	PERPENDICULAR = 		2
}
