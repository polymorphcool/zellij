extends Line2D

signal configuration_updated

onready var glob = get_node( "/root/laces_globals" )

export (float, 1, 200) var init_length = 30
export (float, 1, 200) var init_gap = 15
export (int, 1, 100) var switch_min = 1
export (int, 1, 100) var switch_max = 3
export (int, -1, 500) var width_max = -1
export (int, -1, 500) var height_max = -1
export (int, 1, 100) var segment_max = 30
export (float, 0, 2) var timer = 10
export (bool) var _generate setget generate

var length = init_length
var gap = init_gap
var dir = []
var segment_count = 0
var switch_next = 0
var switch_count = 0

var keepon = true
var wait = 0

var data = []
var center_current = Vector2()
var center_target = Vector2()
var center_anim = true
var last_vp = null

var bbox = {
	'min': Vector2(0,0),
	'max': Vector2(0,0),
	'size': Vector2(0,0),
	'center': Vector2(0,0)
}

# data representation of a line
# warning-ignore:unused_class_variable
onready var conf = glob.line_configuration( init_length, init_gap, width )

func vec2int( v ):
	return Vector2( int(v.x), int(v.y) )

func generate(b):
	_generate = false
	if b:
		reset()

func bbox_reset():
	bbox.min = Vector2(0,0)
	bbox.max = Vector2(0,0)
	bbox.size = Vector2(0,0)
	bbox.center = Vector2(0,0)
	update_gizmos()
	
func bbox_reload():
	bbox_reset()
	for pt in points:
		bbox_add( pt )
	center_target = vec2int(-bbox.center)
	center_anim = true
	update_gizmos()

func bbox_add( v2 ):
	if bbox.min.x > v2.x:
		bbox.min.x = v2.x
	if bbox.min.y > v2.y:
		bbox.min.y = v2.y
	if bbox.max.x < v2.x:
		bbox.max.x = v2.x
	if bbox.max.y < v2.y:
		bbox.max.y = v2.y
	bbox.size = bbox.max - bbox.min
	bbox.center = bbox.min + bbox.size * 0.5
	update_gizmos()

func update_gizmos():
	
	if $rulers != null:
		$rulers.update()
	if $limits != null:
		$limits.set_width_max( conf.width_max )
		$limits.set_height_max( conf.height_max )
		$limits.position = bbox.center

func load_configuration():
	
	switch_min = conf.switch_min
	switch_max = conf.switch_max
	init_length = conf.length
	init_gap = conf.gap
	
	width = conf.width
	joint_mode = conf.joint
	begin_cap_mode = conf.cap
	end_cap_mode = conf.cap
	
	dir = conf.dir.duplicate()
	length = conf.length
	gap = conf.gap
	
	segment_count = 0
	conf.segments = 0
	for sw in conf.switches:
		conf.segments += sw
	conf.segment_max = conf.segments
	segment_max = conf.segment_max
	
	bbox_reset()
	clear_points()
	add_point( Vector2(0,0) )
	
	var l = 1
	var last_was_gap = false
	var lastpt = get_point_position( l - 1 )
	var keepon = true
	
	for si in range(0,len(conf.switches)):
		
		if not keepon:
			break
		
		switch_count = 0
		switch_next = conf.switches[si]
		
		while switch_count < switch_next:
			
			var is_gap = false
			var newpt = Vector2( lastpt.x, lastpt.y )
			if l % 2 == 1:
				if abs(dir[0]) == glob.LACE_DIR_LENGTH: 
					newpt.x += length * ( dir[0]/glob.LACE_DIR_LENGTH )
					dir[0] *= -1
					segment_count += 1
					switch_count += 1
				else:
					newpt.x += gap * ( dir[0]/glob.LACE_DIR_GAP )
					is_gap = true
			else:
				if abs(dir[1]) == glob.LACE_DIR_LENGTH: 
					newpt.y += length * ( dir[1]/glob.LACE_DIR_LENGTH )
					dir[1] *= -1
					segment_count += 1
					switch_count += 1
				else:
					newpt.y += gap * ( dir[1]/glob.LACE_DIR_GAP )
					is_gap = true
			
			var pt_offset = Vector2(0,0)
			if switch_count == switch_next:
				
				var way = 1
				if segment_count % 2 == 0:
					way = -1
				if abs(dir[0]) == glob.LACE_DIR_LENGTH:
					if segment_count < conf.segments:
						pt_offset.x = gap * ( dir[1]/glob.LACE_DIR_GAP ) * way
						newpt.x += pt_offset.x
					var gd = ( dir[1]/glob.LACE_DIR_GAP ) * way
					dir[1] = glob.LACE_DIR_LENGTH * ( dir[0]/glob.LACE_DIR_LENGTH ) * way
					dir[0] = glob.LACE_DIR_GAP * gd
					length = bbox.size.y
				else:
					if segment_count < conf.segments:
						pt_offset.y = gap * ( dir[0]/glob.LACE_DIR_GAP ) * way
						newpt.y += pt_offset.y
					var gd = ( dir[0]/glob.LACE_DIR_GAP ) * way
					dir[0] = glob.LACE_DIR_LENGTH * ( dir[1]/glob.LACE_DIR_LENGTH ) * way
					dir[1] = glob.LACE_DIR_GAP * gd
					length = bbox.size.x
			
			bbox_add( newpt )
			if (conf.width_max > 0 and bbox.size.x > conf.width_max) or (conf.height_max > 0 and bbox.size.y > conf.height_max):
				if last_was_gap:
					remove_point( get_point_count() - 1 )
				elif not is_gap:
					newpt -= pt_offset
					add_point( newpt )
				bbox_reload()
				keepon = false
				break
			
			if lastpt != newpt:
				add_point( newpt )
			l += 1
			last_was_gap = is_gap
			lastpt = newpt
	
	center_target = vec2int(-bbox.center)
	center_anim = true
	
	update_gizmos()
	recenter()
	emit_signal( "configuration_updated" )

func randomise_switch():
	switch_next = int( rand_range( switch_min, switch_max + 1 ) )

func reset():
	
	conf = glob.line_configuration()
	conf.length = init_length
	conf.gap = init_gap
	conf.switches = []
	conf.segment_max = segment_max
	conf.switch_min = switch_min
	conf.switch_max = switch_max
	conf.width_max = width_max
	conf.height_max = height_max
	conf.segments = segment_max
	conf.width = width
	conf.joint = joint_mode
	conf.cap = begin_cap_mode
	var sw_count = 0
	randomise_switch()
	if switch_next == 1:
		switch_next = 2
	while sw_count < segment_max:
		if sw_count + switch_next > segment_max:
			switch_next = segment_max - sw_count
		conf.switches.append( switch_next )
		sw_count += switch_next
		randomise_switch()
	load_configuration()

func _ready():
	randomize()
	reset()
	var vps = get_viewport().size
	center_current = vec2int(vps * 0.5)
	position = center_current

func recenter():
	last_vp = null
	$bg.last_vps = null

func _process(delta):
	
	var vps = get_viewport().size
	if last_vp != vps:
		last_vp = vps
		center_anim = true
	
	if center_anim:
		center_current += vec2int((( vps * 0.5 + center_target) - center_current) * 10 * delta)
		var diff = center_current - position
		if diff.x == 0 and diff.y == 0:
			center_anim = false
		else:
			position = center_current
			if $limits != null and $limits.is_active():
				$limits.position = bbox.center
				
	
	var regenerate = segment_count > conf.segments
	while segment_count > conf.segments:
		var l = len( conf.switches ) - 1
		var lsw = conf.switches[ l ]
		lsw -= 1
		if lsw == 0:
			conf.switches.remove( l )
		else:
			conf.switches[l] = lsw
		segment_count -= 1
	
	if regenerate:
		load_configuration()
