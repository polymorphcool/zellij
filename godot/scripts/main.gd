extends Node2D

onready var glob = get_node( "/root/laces_globals" )
onready var svg = get_node( "/root/svg" )
onready var lfm = get_node( "/root/lfm" )
onready var num_block_tmpl = load( "res://ui/ui_num_block.tscn" )
onready var num_button_tmpl = load( "res://ui/ui_num_button.tscn" )
onready var num_block_script = load( "res://scripts/num_block.gd" )
onready var all_configs = lfm.load_configurations()
onready var current_config = -1

func _ready():
	# configuration
	$ui/columns/bar/info.text = $ui/columns/bar/info.text.replace( "$$version$$", str(glob.LACE_VERSION) )
	# numeric
	$ui/columns/bar/init_gap.set_label( "gap" )
	$ui/columns/bar/init_gap.value_min = 5
	$ui/columns/bar/init_gap.sensitivity = 0.3
	$ui/columns/bar/init_length.set_label( "length" )
	$ui/columns/bar/init_length.value_min = 1
	$ui/columns/bar/init_length.sensitivity = 0.3
	$ui/columns/bar/max_seg.set_label( "count" )
	$ui/columns/bar/max_seg.value_min = 1
	$ui/columns/bar/switch_min.set_label( "switch[min]" )
	$ui/columns/bar/switch_min.value_min = 1
	$ui/columns/bar/switch_max.set_label( "switch[max]" )
	$ui/columns/bar/switch_max.value_min = 2
	
	$ui/columns/bar/limits.set_label( "limits" )
	$ui/columns/bar/limits.value_min = 0
	$ui/columns/bar/limits.value_max = 1
	$ui/columns/bar/limits.sensitivity = 0.03
	$ui/columns/bar/limits.set_value( 0 )
	$ui/columns/bar/width_max.set_label( "width" )
	$ui/columns/bar/width_max.value_min = -1
	$ui/columns/bar/width_max.sensitivity = 0.9
	$ui/columns/bar/width_max.set_value( $lines.width_max )
	$ui/columns/bar/height_max.set_label( "height" )
	$ui/columns/bar/height_max.value_min = -1
	$ui/columns/bar/height_max.sensitivity = 0.9
	$ui/columns/bar/height_max.set_value( $lines.height_max )
	
	$ui/columns/bar/width_max.set_value( 150 )
	$ui/columns/bar/thickness.set_label( "thickness" )
	$ui/columns/bar/thickness.value_min = 1
	#enum
	$ui/columns/bar/joint.set_label( "joint" )
	$ui/columns/bar/joint.value_min = 0
	$ui/columns/bar/joint.value_max = Line2D.LINE_JOINT_ROUND
	$ui/columns/bar/joint.sensitivity = 0.05
	$ui/columns/bar/cap.set_label( "cap" )
	$ui/columns/bar/cap.value_min = 0
	$ui/columns/bar/cap.value_max = Line2D.LINE_CAP_ROUND
	$ui/columns/bar/cap.sensitivity = 0.05
	$ui/columns/bar/level.set_label( "ui level" )
	$ui/columns/bar/level.value_min = 0
	$ui/columns/bar/level.value_max = glob.LACE_RULER_MAX - 1
	$ui/columns/bar/level.sensitivity = 0.05
	$ui/columns/bar/data.set_label( "data" )
	$ui/columns/bar/data.value_min = 0
	$ui/columns/bar/data.value_max = 1
	$ui/columns/bar/data.sensitivity = 0.03
	$ui/columns/bar/load.set_label( "load" )
	config_load()
	
	# adjusting UI
	value_changed( $ui/columns/bar/limits, 0 )
	value_changed( $ui/columns/bar/level, 1 )
	value_changed( $ui/columns/bar/data, 0 )
	
	# signals
	for box in $ui/columns/bar.get_children():
		if box.script == num_block_script:
			# syncing
			sync_display( box )
# warning-ignore:return_value_discarded
			box.connect( "value_changed", self, "value_changed" )
	
# warning-ignore:return_value_discarded
	$ui/columns/bar/generate.connect( "pressed", self, "generate" )
# warning-ignore:return_value_discarded
	$ui/columns/bar/small_btns/generate.connect( "pressed", self, "generate" )
# warning-ignore:return_value_discarded
	$ui/columns/bar/save.connect( "pressed", self, "config_save" )
# warning-ignore:return_value_discarded
	$ui/columns/bar/small_btns/save.connect( "pressed", self, "config_save" )
# warning-ignore:return_value_discarded
	$ui/columns/bar/delete.connect( "pressed", self, "config_delete" )
# warning-ignore:return_value_discarded
	$ui/columns/bar/small_btns/delete.connect( "pressed", self, "config_delete" )
# warning-ignore:return_value_discarded
	$ui/columns/bar/export.connect( "pressed", self, "export_svg" )
# warning-ignore:return_value_discarded
	$ui/columns/bar/small_btns/export.connect( "pressed", self, "export_svg" )
# warning-ignore:return_value_discarded
	$ui/svg_dialog.connect( "confirmed", self, "export_svg_confirmed" )
# warning-ignore:return_value_discarded
	$ui/svg_dialog.connect( "file_selected", self, "export_svg_selected" )
	# grid
	$lines/bg.set_edge( $lines.init_gap )
	# lines
# warning-ignore:return_value_discarded
	$lines.connect( "configuration_updated", self, "configuration_updated" )
	configuration_updated()

# warning-ignore:unused_argument
func _process(delta):
	
	if not $ui/svg_dialog.visible and not $lines.visible:
		$lines.visible = true
	
	if $ui/svg_dialog.visible:
		var vps = get_viewport().size
		$ui/svg_dialog.rect_size = vps

func _input(event):
	if event is InputEventKey and event.pressed : 
		if event.scancode == KEY_ESCAPE:
			if $ui/svg_dialog.visible:
				$ui/svg_dialog.visible = false
			else:
				get_tree().quit()
		if not $ui/svg_dialog.visible:
			if event.scancode == KEY_Q:
				get_tree().quit()
			elif event.scancode == KEY_G:
				generate()
			elif event.scancode == KEY_S:
				config_save()
			elif event.scancode == KEY_X:
				config_delete()
			elif event.scancode == KEY_UP:
				config_next()
			elif event.scancode == KEY_DOWN:
				config_previous()
			elif event.scancode == KEY_E:
				export_svg()

func configuration_updated():
	
	var switches = $lines.conf.switches
	
	if $ui/columns/config.get_child_count() == len(switches) + 2:
		var id = 1
		for i in switches:
			$ui/columns/config.get_child( id ).set_value( i )
			id += 1
		return
	
	# wrong number of num_boxes, trashing everything and recreation
	while $ui/columns/config.get_child_count() > 1:
		var last = $ui/columns/config.get_child_count() - 1
		$ui/columns/config.remove_child( $ui/columns/config.get_child( last ) )
	
	var count = 0
	for i in switches:
		var nblock = num_block_tmpl.instance()
		if count == 0:
			nblock.value_min = 2
		else:
			nblock.value_min = 0
		nblock.set_label( "" )
		nblock.set_value( i )
# warning-ignore:return_value_discarded
		nblock.connect( "value_changed", self, "configuration_changed" )
		$ui/columns/config.add_child( nblock )
		count += 1
	
	var addbtn = num_button_tmpl.instance()
	addbtn.text = "+"
# warning-ignore:return_value_discarded
	addbtn.connect( "pressed", self, "add_switch" )
	$ui/columns/config.add_child( addbtn )
	addbtn.rect_size.x = 20

func add_switch():
	$lines.conf.switches.append( 0 )
	configuration_updated()

# warning-ignore:unused_argument
# warning-ignore:unused_argument
func configuration_changed( box, value ):
	var swicthes = []
	for nblock in $ui/columns/config.get_children():
		if nblock.script == num_block_script:
			swicthes.append( nblock.value )
	$lines.conf.switches = swicthes
	$lines.load_configuration()
	$lines.conf.segment_max = $lines.conf.segments
	$ui/columns/bar/max_seg.set_value( $lines.conf.segments )
	config_check()

func config_next():
	if all_configs == null or all_configs.empty():
		return
	current_config += 1
	if current_config > len( all_configs ) - 1:
		current_config = len( all_configs ) - 1
	config_load( current_config )
	sync_display( $ui/columns/bar/load )

func config_previous():
	if all_configs == null or all_configs.empty():
		return
	current_config -= 1
	if current_config < 0:
		current_config = 0
	config_load( current_config )
	sync_display( $ui/columns/bar/load )

func config_load( id = null ):
	
	if id != null and current_config != id:
		if id != -1:
			current_config = id
			$lines.conf = all_configs[current_config].duplicate(true)
			glob.check_configuration( $lines.conf )
			$lines.load_configuration()
		else:
			if current_config != -1:
				generate()
			current_config = id
		if $lines.conf.width_max > 0 or $lines.conf.height_max > 0:
			print( "activate limits" )
			$ui/columns/bar/limits.set_value( 1 )
			value_changed( $ui/columns/bar/limits, 1 )
		else:
			print( "deactivate limits" )
			$ui/columns/bar/limits.set_value( 0 )
			value_changed( $ui/columns/bar/limits, 0 )
	
	if all_configs == null or all_configs.empty():
		$ui/columns/bar/load.value_min = -1
		$ui/columns/bar/load.value_max = -1
	else:
		$ui/columns/bar/load.value_min = 0
		$ui/columns/bar/load.value_max = len( all_configs ) - 1
	
	$lines/bg.set_edge( $lines.init_gap )

func config_save():
	if current_config != -1:
		return
	if all_configs == null:
		all_configs = [] 
	all_configs.append( $lines.conf.duplicate(true) )
	current_config = len( all_configs ) - 1
	$ui/columns/bar/load.set_value( current_config )
	$ui/columns/bar/load.value_max = current_config
	lfm.save_configurations( all_configs )
	config_load()
	sync_display( $ui/columns/bar/load )

func config_delete():
	if current_config == -1:
		return
	all_configs.erase( all_configs[ current_config ] )
	current_config -= 1
	if current_config > len( all_configs ) - 1:
		current_config = len( all_configs ) - 1
	lfm.save_configurations( all_configs )
	config_load( current_config )
	$lines.load_configuration()
	sync_display( $ui/columns/bar/load )

func config_check():
	if current_config == -1 and (all_configs == null or all_configs.empty()):
		return
	if current_config == -1:
		var cid = 0
		for cfg in all_configs:
			if glob.compare_configurations( $lines.conf, cfg ):
				current_config = cid
				break
			cid += 1
	else:
		var cfg = all_configs[ current_config ]
		if not glob.compare_configurations( $lines.conf, cfg ):
			current_config = -1
	sync_display( $ui/columns/bar/load )

func export_svg():
	var vps = get_viewport().size
	$ui/svg_dialog.rect_position = Vector2(0,0)
	$ui/svg_dialog.rect_size = vps
	$ui/svg_dialog.update()
	$ui/svg_dialog.show()
	$lines.visible = false

# warning-ignore:unused_argument
func export_svg_selected(f):
	export_svg_confirmed()

func export_svg_confirmed():
	
	var filepath = $ui/svg_dialog.current_path
	if filepath.substr( len(filepath)-4, 4) != ".svg":
		filepath += ".svg"
	
	var content = svg.generate_content( $lines )
	var file = File.new()
	var err = file.open( filepath, File.WRITE )
	if err == OK:
		file.store_string(content)
		file.close()
	else:
		print( "save failed: " + filepath )
	
	$lines.visible = true

func generate():
	$lines.reset()
	$lines/bg.set_edge( $lines.init_gap )
	config_check()
	sync_display_all()

func value_changed( box, value ):
	
	# generation
	if box == $ui/columns/bar/max_seg:
		$lines.segment_max = value
		$lines.conf.segment_max = value
		config_check()
	elif box == $ui/columns/bar/switch_min:
		$lines.switch_min = value
		$lines.conf.switch_min = value
		config_check()
	elif box == $ui/columns/bar/switch_max:
		$lines.switch_max = value
		$lines.conf.switch_max = value
		config_check()
	
	elif box == $ui/columns/bar/limits:
		$ui/columns/bar/width_max.disable( value == 0 )
		$ui/columns/bar/height_max.disable( value == 0 )
		if value == 0:
			$lines.width_max = -1
			$lines.height_max = -1
			$lines.conf.width_max = -1
			$lines.conf.height_max = -1
			$lines/limits.set_width_max( value )
			$lines/limits.set_height_max( value )
			$lines.load_configuration()
			config_check()
		else:
			if $lines.conf.width_max > 0 or $lines.conf.height_max > 0:
				$ui/columns/bar/width_max.set_value( $lines.conf.width_max )
				$ui/columns/bar/height_max.set_value( $lines.conf.height_max )
			value_changed( $ui/columns/bar/width_max, $ui/columns/bar/width_max.value )
			value_changed( $ui/columns/bar/height_max, $ui/columns/bar/height_max.value )
			
	elif box == $ui/columns/bar/width_max:
		$lines.width_max = value
		$lines.conf.width_max = value
		$lines/limits.set_width_max( value )
		$lines.load_configuration()
		config_check()
	elif box == $ui/columns/bar/height_max:
		$lines.height_max = value
		$lines.conf.height_max = value
		$lines/limits.set_height_max( value )
		$lines.load_configuration()
		config_check()
	
	# configuration
	elif box == $ui/columns/bar/init_length:
		$lines.init_length = value
		$lines.conf.length = value
		$lines.load_configuration()
		config_check()
	elif box == $ui/columns/bar/init_gap:
		$lines.init_gap = value
		$lines.conf.gap = value
		$lines/bg.set_edge( value )
		$lines.load_configuration()
		config_check()
	elif box == $ui/columns/bar/thickness:
		# direct, avoid reload
		$lines.width = value
		$lines.conf.width = value
		$lines/rulers.update()
		config_check()
	elif box == $ui/columns/bar/joint:
		# direct, avoid reload
		$lines.joint_mode = value
		$lines.conf.joint = value
		config_check()
	elif box == $ui/columns/bar/cap:
		# direct, avoid reload
		$lines.begin_cap_mode = value
		$lines.end_cap_mode = value
		$lines.conf.joint = value
		config_check()
	
	elif box == $ui/columns/bar/level:
		for c in $ui/columns/bar.get_children():
			if c is Label or c is Button:
				c.visible = value != 0
		for c in $ui/columns/config.get_children():
			if c is Label or c is Button:
				c.visible = value != 0
		$ui/columns/bar/small_btns.visible = value == 0
		$lines/bg.visible = value != 0
		$lines/rulers.set_display( value )
		$lines/limits.visible = value != 0
		if value == 1:
			$ui/columns/bar/generate.text = 'generate'
			$ui/columns/bar/save.text = 'save'
			$ui/columns/bar/delete.text = 'delete'
			$ui/columns/bar/export.text = 'svg'
		elif value == 2:
			$ui/columns/bar/generate.text = 'generate (s)'
			$ui/columns/bar/save.text = 'save (s)'
			$ui/columns/bar/delete.text = 'delete (x)'
			$ui/columns/bar/export.text = 'svg (s)'
	
	elif box == $ui/columns/bar/data:
		$ui/columns/config.visible = value == 1
	
	elif box == $ui/columns/bar/load:
		config_load( value )
		sync_display_all()

func sync_display_all():
	for box in $ui/columns/bar.get_children():
		sync_display( box )

func sync_display( box ):
	
	# generation
	if box == $ui/columns/bar/max_seg:
		box.set_value($lines.conf.segment_max)
	elif box == $ui/columns/bar/switch_min:
		box.set_value($lines.conf.switch_min)
	elif box == $ui/columns/bar/switch_max:
		box.set_value($lines.conf.switch_max)
	elif box == $ui/columns/bar/width_max:
		box.disable( $ui/columns/bar/limits.value == 0 )
		if $ui/columns/bar/limits.value == 1:
			box.set_value($lines.conf.width_max)
	elif box == $ui/columns/bar/height_max:
		box.disable( $ui/columns/bar/limits.value == 0 )
		if $ui/columns/bar/limits.value == 1:
			box.set_value($lines.conf.height_max)
	
	# configuration
	elif box == $ui/columns/bar/init_length:
		box.set_value($lines.conf.length)
	elif box == $ui/columns/bar/init_gap:
		box.set_value($lines.conf.gap)
	elif box == $ui/columns/bar/thickness:
		box.set_value($lines.conf.width)
	elif box == $ui/columns/bar/joint:
		box.set_value($lines.conf.joint)
	elif box == $ui/columns/bar/cap:
		box.set_value($lines.conf.cap)
	
	elif box == $ui/columns/bar/level:
		box.set_value($lines/rulers.display)
	elif box == $ui/columns/bar/data:
		if $ui/columns/config.visible:
			box.set_value(1)
		else:
			box.set_value(0)
	elif box == $ui/columns/bar/load:
		box.set_value(current_config)
		$ui/columns/bar/save.disabled = current_config != -1
		$ui/columns/bar/delete.disabled = current_config == -1
