tool

extends Node2D


export(float,0,500) var _radius:float = 60 setget set_radius
export(int,2,100) var _edges:int = 6 setget set_edges
export(float,0,1) var _random:float = 0 setget set_random
export(float,0,5) var _random_min:float = 2
export(float,0,5) var _random_max:float = 2
export(bool) var _generate:bool = false setget set_generate
export(bool) var _draw_vertex:bool = false
export(bool) var _draw_normal:bool = false
export(bool) var _edge_id:bool = false
export(int,-1,100) var _display_clone:int = -1 setget set_display_clone
export(float,0,5) var _offset_clone:float = 5 setget set_offset_clone

var vertices_raw:Array = []
var symetric_axis:Array = []
var shape:Dictionary = {} # main shape
var symetric:Dictionary = {} # a symetric shape
var clones:Array = []

func set_radius(f:float) -> void:
	_radius = f
	if is_inside_tree():
		refresh()

func set_edges(i:int) -> void:
	_edges = i
	generate()

func set_random(f:float) -> void:
	_random = f
	generate()

func set_generate(b:bool) -> void:
	_generate = false
	if b:
		generate()

func set_display_clone(i:int) -> void:
	_display_clone = i
	if "edges" in shape.keys():
		_display_clone %= shape.edges.size()
	if is_inside_tree():
		refresh()

func set_offset_clone(f:float) -> void:
	_offset_clone = f
	if is_inside_tree():
		refresh()

# CREATION ######################

func new_shape() -> Dictionary:
	return {
		'vertices': [],
		'edges': [],
		'valid': false
	}

func new_vertex( pos:Vector2, norm:Vector2 ) -> Dictionary:
	var v:Dictionary = {
		'pos': pos,
		'normal': norm
	}
	return v

func rotated_vertex( v:Dictionary, rot:float ) -> Dictionary:
	return {
		'pos': v.pos.rotated( rot ),
		'normal': v.normal.rotated( rot )
	}

func clone_vertex( v:Dictionary ) -> Dictionary:
	return {
		'pos': v.pos,
		'normal': v.normal
	}

func new_edge( uid:int, v0:Dictionary, v1:Dictionary ) -> Dictionary:
	var e:Dictionary = {
		'uid': uid,
		'v0': v0,
		'v1': v1,
		'dir': null,
		'normal': null,
		'length': 0,
	}
	e.dir = (v1.pos-v0.pos)
	e.mid = v0.pos + e.dir * 0.5
	e.length = e.dir.length()
	e.dir /= e.length
	e.normal = Vector2( e.dir.y, -e.dir.x )
	return e

func mirror_vertices( source:Dictionary, dir:Vector2 ) -> Array:
	var normal:Vector2 =  Vector2( dir.y, -dir.x )
	var cloned_vertices:Array = []
	for v in source.vertices:
		var proj_v:Dictionary = clone_vertex( v )
		proj_v.pos = ( dir * dir.dot(v.pos) - normal * normal.dot(v.pos) )
		cloned_vertices.append( proj_v )
	var output:Array = []
	var vcount:int = source.vertices.size()
	for i in range( 0, vcount ):
		output.append( cloned_vertices[vcount-(i+1)] )
	return output

func compare_vertices( vertices0:Array, vertices1:Array ) -> bool:
	var vcount:int = vertices0.size()
	if vcount == 0 or vcount != vertices1.size():
		return false
	var v1offset:int = -1
	for i in range(0,vcount):
		if vertices1[i].pos.distance_squared_to( vertices0[0].pos ) <= 1e-3:
			v1offset = i
			break
	if v1offset == -1:
		return false
	for i in range(0,vcount):
		var v0 = vertices0[i].pos
		var v1 = vertices1[(i+v1offset)%vcount].pos
		if v0.distance_squared_to( v1 ) > 1e-3:
			return false
	return true

func generate_edges() -> void:
	
	# first: optimise vertices!
	for i in range( 0, vertices_raw.size() ):
		var h = (i+vertices_raw.size()-1) % vertices_raw.size()
		var j = (i+1) % vertices_raw.size()
		var prev_dir:Vector2 = (vertices_raw[i]-vertices_raw[h]).normalized()
		var curr_dir:Vector2 = (vertices_raw[j]-vertices_raw[i]).normalized()
		if prev_dir.dot(curr_dir) >= 1 - 1e-5:
			continue
		var prev_norm:Vector2 = Vector2( prev_dir.y, -prev_dir.x )
		var next_norm:Vector2 = Vector2( curr_dir.y, -curr_dir.x )
		var norm:Vector2 = (prev_norm+next_norm).normalized()
		shape.vertices.append( new_vertex( vertices_raw[i], norm ) )
	
	for i in range(0,shape.vertices.size()):
		var j = (i+1) % shape.vertices.size()
		shape.edges.append( new_edge( i, shape.vertices[i], shape.vertices[j] ) )

func compute_symetry_axis() -> void:
	symetric_axis = []
	for v in shape.vertices:
		var dir:Vector2 = v.pos.normalized()
		var mirrored:Array = mirror_vertices( shape, dir )
		if compare_vertices( mirrored, shape.vertices ):
			symetric_axis.append( dir )
	for e in shape.edges:
		var dir:Vector2 = e.mid.normalized()
		var mirrored:Array = mirror_vertices( shape, dir )
		if compare_vertices( mirrored, shape.vertices ):
			symetric_axis.append( dir )

func generate_symetric( scale:Vector2 ) -> void:
	symetric = new_shape()
	var flipped_vertices:Array = []
	for v in shape.vertices:
		var fv:Dictionary = clone_vertex( v )
		fv.pos *= scale
		fv.normal *= scale
		flipped_vertices.append( fv )
	var vcount:int = flipped_vertices.size()
	for i in range( 0, vcount ):
		var vid:int = ( vcount - i ) % vcount
		symetric.vertices.append( flipped_vertices[ vid ] )
	if compare_vertices( shape.vertices, symetric.vertices ):
		symetric.vertices = []
		return
	for i in range( 0, vcount ):
		var vid:int = ( vcount - i ) % vcount
		var nid = (vid+(vcount-1)) % vcount
		var e:Dictionary = new_edge( i, symetric.vertices[vid], symetric.vertices[nid] )
		# flipping normal!
		e.normal *= -1
		symetric.edges.append( e )
	symetric.valid = true

func new_clone() -> Dictionary:
	return {
		'vertices': [],
		'symetry': false,
		'valid': false
	}

func clone_shape( source:Dictionary, rot:float, offset:Vector2 ) -> Dictionary:
	var clone:Dictionary = new_clone()
	if source == symetric:
		clone.symetry = true
	for v in source.vertices:
		var cv:Dictionary = rotated_vertex( v, rot )
		cv.pos += offset
		clone.vertices.append( cv )
	for eclones in clones:
		for other in eclones:
			if compare_vertices( other.vertices, clone.vertices ):
				clone.vertices = []
				return clone
	clone.valid = true
	return clone

func generate() -> void:
	
	if not is_inside_tree():
		return
	
	# cleanup
	vertices_raw = []
	shape = new_shape()
	symetric = new_shape()
	clones = []
	
	randomize()
	var agap:float = TAU / _edges
	for i in range(0,_edges):
		var a:float = i * agap
		var vert:Vector2 = Vector2(cos(a),sin(a))
		if rand_range(0,1) < _random:
			vert *= rand_range( _random_min, _random_max )
		vertices_raw.append( vert )
	
	generate_edges()
	compute_symetry_axis()
	
	if symetric_axis.empty():
		generate_symetric( Vector2(1,-1) )
	
	var clone_eid:int = 0
	for e in shape.edges:
		clones.append([])
		for ee in shape.edges:
			if abs(e.length - ee.length) < 1e-5:
				var rot:float = PI + ee.normal.angle_to( e.normal )
				var clone = clone_shape( shape, rot, e.mid - ee.mid.rotated( rot ) )
				if clone.valid:
					clones[clone_eid].append( clone )
		for ee in symetric.edges:
			if abs(e.length - ee.length) < 1e-5:
				var rot:float = PI + ee.normal.angle_to( e.normal )
				var clone = clone_shape( symetric, rot, e.mid - ee.mid.rotated( rot ) )
				if clone.valid:
					clones[clone_eid].append( clone )
		clone_eid += 1
	
	set_display_clone( _display_clone )
	refresh()

# DISPLAY ######################

func refresh() -> void:
	
	var polygon:Array = []
	var polygon_sym:Array = []
	while $basis.get_child_count() > 0:
		$basis.remove_child( $basis.get_child(0) )
	while $symetric.get_child_count() > 0:
		$symetric.remove_child( $symetric.get_child(0) )
	while $symetry_axis.get_child_count() > 0:
		$symetry_axis.remove_child( $symetry_axis.get_child(0) )
	while $clones.get_child_count() > 0:
		$clones.remove_child( $clones.get_child(0) )
	
	var vertl:Line2D = Line2D.new()
	vertl.width = 1
	vertl.add_point( Vector2(-3,-3) )
	vertl.add_point( Vector2(3,3) )
	vertl.add_point( Vector2(0,0) )
	vertl.add_point( Vector2(3,-3) )
	vertl.add_point( Vector2(-3,3) )
	vertl.default_color = Color.red
	
	if _draw_vertex:
		for v in vertices_raw:
			var vl:Line2D = vertl.duplicate()
			$basis.add_child( vl )
			vl.position = v * _radius
	
	vertl.clear_points()
	vertl.add_point( Vector2(-5,0) )
	vertl.add_point( Vector2(5,0) )
	vertl.add_point( Vector2(0,0) )
	vertl.add_point( Vector2(0,-5) )
	vertl.add_point( Vector2(0,5) )
	vertl.default_color = Color.green
	
	var normal:Line2D = Line2D.new()
	normal.width = 1
	normal.add_point( Vector2(0,0) )
	normal.add_point( Vector2(20,0) )
	normal.default_color = Color(1,0,1,0.7)
	
	for v in shape.vertices:
		var pos:Vector2 = v.pos * _radius
		if _draw_vertex:
			var vl:Line2D = vertl.duplicate()
			$basis.add_child( vl )
			vl.position = pos
		if _draw_normal:
			var nl:Line2D = normal.duplicate()
			$basis.add_child( nl )
			nl.position = pos
			nl.rotation = Vector2.RIGHT.angle_to( v.normal )
		polygon.append( pos )
	
	for v in symetric.vertices:
		var pos:Vector2 = v.pos * _radius
		if _draw_vertex:
			var vl:Line2D = vertl.duplicate()
			$symetric.add_child( vl )
			vl.position = pos
		if _draw_normal:
			var nl:Line2D = normal.duplicate()
			$symetric.add_child( nl )
			nl.position = pos
			nl.rotation = Vector2.RIGHT.angle_to( v.normal )
		polygon_sym.append( pos )
	
	normal.clear_points()
	normal.add_point( Vector2(0,0) )
	normal.add_point( Vector2(10,0) )
	
	for e in shape.edges:
		if _draw_normal:
			var nl:Line2D = normal.duplicate()
			$basis.add_child( nl )
			nl.position = e.mid * _radius
			nl.rotation = Vector2.RIGHT.angle_to( e.normal )
		if _edge_id:
			var lbl:Label = $lbl.duplicate()
			lbl.set( "custom_colors/font_color", $basis.color )
			lbl.text = str( e.uid )
			$basis.add_child( lbl )
			lbl.rect_position = e.mid * _radius + e.normal * 20 - lbl.rect_size * 0.5
			lbl.visible = true
	for e in symetric.edges:
		if _draw_normal:
			var nl:Line2D = normal.duplicate()
			$symetric.add_child( nl )
			nl.position = e.mid * _radius
			nl.rotation = Vector2.RIGHT.angle_to( e.normal )
		if _edge_id:
			var lbl:Label = $lbl.duplicate()
			lbl.set( "custom_colors/font_color", $symetric.default_color )
			lbl.text = str( e.uid )
			$symetric.add_child( lbl )
			lbl.rect_position = e.mid * _radius + e.normal * 30 - lbl.rect_size * 0.5
			lbl.visible = true
	
	vertl.queue_free()
	normal.queue_free()
	
	$basis.polygon = polygon
	if !polygon_sym.empty():
		polygon_sym.append( polygon_sym[0] )
	$symetric.points = polygon_sym
	
	var syml:Line2D = Line2D.new()
	syml.width = 1
	syml.default_color = Color( 1,1,0,0.8 )
	for axis in symetric_axis:
		var al:Line2D = syml.duplicate()
		al.add_point( axis * _radius * _random_max )
		al.add_point( axis * -_radius * _random_max )
		$symetry_axis.add_child( al )
	
	var clonep:Polygon2D = Polygon2D.new()
	clonep.color = Color( 1,1,1,0.2 )
	var clonel:Line2D = Line2D.new()
	clonel.width = 1
	clonel.default_color = Color( 0.5,0,1,0.8 )
	for ci in range( 0, clones.size() ):
		if _display_clone != -1 and ci != _display_clone:
			continue
		var edge_clones:Array = clones[ci]
		var clone_offset:Vector2 = Vector2.ZERO
		for clone in edge_clones:
			clone_offset += shape.edges[ci].normal * _offset_clone
	#		var cp:Polygon2D = clonep.duplicate()
			var cl:Line2D = clonel.duplicate()
	#		$clones.add_child( cp )
			$clones.add_child( cl )
			if clone.symetry:
				cl.default_color = Color( 0,0.75,1,0.8 )
			polygon = []
			for i in range(0,clone.vertices.size()+1):
				var vi:int = i % clone.vertices.size()
				var v:Vector2 = clone.vertices[vi].pos * _radius
				if i < clone.vertices.size():
					polygon.append( v )
				cl.add_point( v )
	#		cp.polygon = polygon
			cl.position = clone_offset
#			cp.position = clone_offset
		
	clonep.queue_free()
	clonel.queue_free()

# GENERAL ######################

func _ready() -> void:
	generate()

func _input(event) -> void:
	if event is InputEventKey and event.pressed:
		match event.scancode:
			KEY_SPACE:
				generate()
			KEY_UP:
				set_edges( _edges + 1 )
			KEY_DOWN:
				set_edges( max( 3, _edges - 1 ) )
			KEY_RIGHT:
				set_display_clone( _display_clone + 1 )
			KEY_LEFT:
				set_display_clone( max( -1, _display_clone - 1 ) )
