shader_type canvas_item;
render_mode blend_mix;

const float _PI = 3.14159265359;
uniform float uv_scale:hint_range(0,20);
uniform float offset:hint_range(0,2);

void fragment() {
	vec2 fuv0 = UV*uv_scale;
	int row = int(fuv0.y);
	if (row%2 == 1) { fuv0.x += 0.5; }
	fuv0.y += ( 1.0 - sin(_PI/3.0) ) * 2.0 * float(row);
	vec2 fuv1 = fuv0;
	row += 1;
	if (row%2 == 1) { fuv1.x += 0.5; }
	fuv1.y += ( 1.0 - sin(_PI/3.0) ) * 2.0;
	vec4 c0 = texture(TEXTURE,fuv0);
	vec4 c1 = texture(TEXTURE,fuv1);
	if (c0.a > c1.a) {
		COLOR = c0;
	} else {
		COLOR = c1;
	}
//	COLOR = mix( c0, c1, c1.a * 1.0 );
	COLOR = c0;
}