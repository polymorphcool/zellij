extends Node

const collision_error_margin:float = 1e-1

export(int,0,500) var radius:float = 60 setget set_radius
export(int,2,100) var _edges:int = 6 setget set_edges
export(int,0,4) var sub_level:int = 1 setget set_sub_level
export(bool) var enable_random:bool = false setget set_enable_random

var vertices:Array = []
var vertex_normals:Array = []
var collision_vertices:Array = []
var edges:Array = []
var edge_pairs:Dictionary = {}
var collisions:Dictionary = {}
var levels:Dictionary = {}

func set_radius(f:float) -> void:
	radius = f
	generate()

func set_edges(i:int) -> void:
	_edges = i
	generate()

func set_sub_level(i:int) -> void:
	sub_level = i
	generate()

func set_enable_random(b:bool) -> void:
	enable_random = b
	if b:
		generate()
		enable_random = false

func new_edge( pt0:Vector2, pt1:Vector2 ) -> Dictionary:
	var e:Dictionary = {
		'pt0': pt0,
		'pt1': pt1,
		'mid': null,
		'dir': null,
		'length': 0.0,
		'normal': null,
	}
	e.dir = (pt1-pt0)
	e.mid = pt0 + e.dir * 0.5
	e.length = e.dir.length()
	e.dir /= e.length
	e.normal = Vector2( e.dir.y, -e.dir.x )
	return e

func area_entered( local:Area2D, foreign:Area2D ) -> void:
	if not local in collisions.keys():
		collisions[ local ] = []
	collisions[ local ].append( foreign )

func generate_around( ref:Node, lvl:int ) -> void:
	
	var hgap:float = 1.0 / (edge_pairs.size()+1)
	var agap:float = 1.0 / sub_level
	var a:float = 0.5 + agap * 0.5 * (sub_level-max(0,lvl-1))
	var h:float = 0
	
	# creation of clones
	for local_edge in edge_pairs.keys():
		
		if edge_pairs[local_edge].size() == 0:
			continue
		
		var clone:Area2D = $area.duplicate()
		clone.get_child(0).polygon = collision_vertices
		clone.name = "lvl " + str(lvl) + " i" + str( $clones.get_child_count() )
		clone.get_child(1).text = clone.name
		clone.visible = true
		$clones.add_child( clone )
		clone.get_child(1).rect_position = clone.get_child(1).rect_size * -0.5
		
		var rot:float = TAU
		var dot:float = 1
		var mid:Vector2
		for i in range(0, edge_pairs[local_edge].size() ):
			var foreign_edge:Dictionary = edge_pairs[local_edge][i]
			var d:float = local_edge.normal.dot( foreign_edge.normal )
			var r:float = foreign_edge.dir.angle_to( local_edge.dir * -1 ) + ref.global_rotation
			if dot > d or ( abs(dot-d) < 1e-5 and r > rot ):
				dot = d
				rot = r
				mid = foreign_edge.mid
		
		var offset:Vector2 = (local_edge.mid).rotated( ref.global_rotation )
		offset -= mid.rotated( rot )
		clone.global_position = ref.global_position + offset
		
		clone.global_rotation = rot
		
		clone.connect( "smart_area_entered", self, "area_entered" )
		var cdisplay:Polygon2D = Polygon2D.new()
		cdisplay.color = Color.from_hsv( h, 1, 1, a )
		cdisplay.polygon = vertices
		clone.add_child( cdisplay )
		var lbl = clone.get_child(1)
		lbl.get_parent().remove_child(lbl)
		clone.add_child( lbl )
		
#		var midl:Line2D = Line2D.new()
#		midl.width = 2
#		midl.default_color = Color(0.5,0.5,0.5)
#		midl.add_point( mid + Vector2(-5,-5) )
#		midl.add_point( mid + Vector2(5,5) )
#		midl.add_point( mid + Vector2(-5,5) )
#		midl.add_point( mid + Vector2(5,-5) )
#		clone.add_child( midl )
#		midl.global_rotation = clone.global_rotation
		
		levels[lvl].append( clone )
		h += hgap

func generate() -> void:
	
	if not is_inside_tree():
		return
	
	#cleanup
	while $clones.get_child_count() > 0:
		$clones.remove_child( $clones.get_child(0) )
	vertices = []
	collision_vertices = []
	vertex_normals = []
	edges = []
	edge_pairs = {}
	levels = {}
	var agap:float = TAU / _edges
	for i in range(0,_edges):
		var a:float = i * agap
		var vert:Vector2 = Vector2(cos(a),sin(a)) * radius
		if enable_random and rand_range(0,1) > 0.5:
			vert *= 2
		vertices.append( vert )
		collision_vertices.append( vert )
		vertex_normals.append( Vector2.ZERO )
	
	for i in range(0,_edges):
		var j:int = (i+1) % _edges
		var e:Dictionary = new_edge( vertices[i], vertices[j] )
		vertex_normals[i] += e.normal
		vertex_normals[j] += e.normal
		edges.append( e )
	
	for i in range(0,_edges):
		vertex_normals[i] = vertex_normals[i].normalized()
		collision_vertices[i] -= vertex_normals[i] * collision_error_margin
	
	$base.polygon = vertices
	
	# making pairs
	for e_local in edges:
		edge_pairs[ e_local ] = [e_local]
		for e in edges:
			if e == e_local:
				continue
			if abs( e.length - e_local.length ) < 1e-5:
				edge_pairs[ e_local ].append( e )
	
	var main:Area2D = $area.duplicate()
	main.get_child(0).polygon = collision_vertices
	main.name = "main " + str( _edges )
	main.get_child(1).text = main.name
	main.visible = true
	$clones.add_child( main )
	main.get_child(1).rect_position = main.get_child(1).rect_size * -0.5
	main.position = $base.position
	main.connect( "smart_area_entered", self, "area_entered" )
	
	levels[0] = [main]
	for i in range( 0, sub_level ):
		levels[i+1] = []
		for node in levels[i]:
			generate_around( node, i+1 )
	
func _ready() -> void:
	$base.position = get_viewport().size * 0.5
	generate()

func _input(event) -> void:
	
	if event is InputEventKey and event.pressed:
		match event.scancode:
			KEY_UP:
				set_edges( _edges + 1 )
			KEY_DOWN:
				set_edges( max( 3, _edges - 1 ) )
			KEY_LEFT:
				set_sub_level( max( 0, sub_level - 1 ) )
			KEY_RIGHT:
				set_sub_level( min( 4, sub_level + 1 ) )
			KEY_R:
				set_enable_random(true)

func _process(delta) -> void:
	
	if not collisions.empty():
		var zombies:Array = []
		for area in $clones.get_children():
			if area in zombies:
				continue
			if not area in collisions:
				continue
			var colls = collisions[area]
			for c in colls:
				if c == area:
					continue
				if not c in zombies:
					zombies.append(c)
		for z in zombies:
			$clones.remove_child( z )
		collisions = {}
