extends Area2D

signal smart_area_entered
signal smart_area_exited

func _on_area_entered(area: Area2D) -> void:
	emit_signal( "smart_area_entered", self, area )

func _on_area_exited(area: Area2D) -> void:
	emit_signal( "smart_area_exited", self, area )

func _ready():
	connect( "area_entered", self, "_on_area_entered" )
	connect( "area_exited", self, "_on_area_exited" )
