extends Node2D

onready var zellij:Node = get_node("/root/zellij")
onready var tiler:Node = get_node("/root/zellij_tiler")

export(float,0,500) var _radius:float = 60 setget set_radius
export(int,2,100) var _edges:int = 6 setget set_edges
export(int,0,10) var _depth:int = 2 setget set_depth
export(float,0,1) var _random:float = 0 setget set_random
export(float,0,5) var _random_min:float = 2
export(float,0,5) var _random_max:float = 2

export(int,0,4) var _edge_sorting:int = 0 setget set_edge_sorting
export(int,0,2) var _clone_sorting:int = 0 setget set_clone_sorting

var vertices_raw:Array = []
var shape:Dictionary = {}
var tiling:Dictionary = {}
var tiling_processing:bool = false
var tiling_curr_depth:int = 0

func set_radius(f:float) -> void:
	_radius = f
	if is_inside_tree():
		refresh()

func set_edges(i:int) -> void:
	_edges = i

func set_depth(i:int) -> void:
	_depth = i

func set_random(f:float) -> void:
	_random = f

func set_edge_sorting(i:int) -> void:
	_edge_sorting = i % 4

func set_clone_sorting(i:int) -> void:
	_clone_sorting = i % 3

# CREATION ######################

func generate_shape() -> void:
	# cleanup
	vertices_raw = []
	randomize()
	var agap:float = TAU / _edges
	for i in range(0,_edges):
		var a:float = i * agap
		var vert:Vector2 = Vector2(cos(a),sin(a))
		if rand_range(0,1) < _random:
			vert *= rand_range( _random_min, _random_max )
		vertices_raw.append( vert )
	# this will generates a signal
	tiler.create_shape( self, [vertices_raw] ) # require list of list of vertices

func generate_tiling() -> void:
	self.tiling = {}
	tiling_curr_depth = 0
	refresh()
	tiler.create_tiling( self, shape, _depth, _edge_sorting, _clone_sorting )
	tiling_processing = true
	
# SIGNALS ######################

func shape_validated( caller:Node, shape:Dictionary ) -> void:
	if self != caller:
		return
	self.shape = shape
	if self.shape.valid:
		generate_tiling()
	else:
		purge()

func tiling_step( caller:Node, tiling:Dictionary ) -> void:
	if self != caller:
		return
	self.tiling = tiling
	update_tiling_display()

func tiling_finished( caller:Node, tiling:Dictionary ) -> void:
	if self != caller:
		return
	self.tiling = tiling
	tiling_processing = false
	update_tiling_display()

# DISPLAY ######################

func purge() -> void:
	while $basis.get_child_count() > 0:
		$basis.remove_child( $basis.get_child(0) )
	while $tiles.get_child_count() > 0:
		$tiles.remove_child( $tiles.get_child(0) )
	while $progress_bars.get_child_count() > 0:
		$progress_bars.remove_child( $progress_bars.get_child(0) )

func info() -> void:
	
	$info.text = "radius " + str(_radius) + "\n"
	$info.text += "edges " + str(_edges) + " (request)\n"
	$info.text += "edges " + str(shape.edges.size()) + " (actual)\n"
	var clone_count:int = 0
	var valid_clone_count:int = 0
	for ec in shape.clones:
		for c in ec:
			clone_count += 1
			if c.valid:
				valid_clone_count += 1
	$info.text += "clones " + str(valid_clone_count) + " (valids)\n"
	$info.text += "clones " + str(clone_count) + " (total)\n"
	match _edge_sorting:
		zellij.EDGE_SORT.DEFAULT:
			$info.text += "edge sorting: creation order\n"
		zellij.EDGE_SORT.INVERSE:
			$info.text += "edge sorting: inverse of creation order\n"
		zellij.EDGE_SORT.SMALL:
			$info.text += "edge sorting: small first\n"
		zellij.EDGE_SORT.BIG:
			$info.text += "edge sorting: big first\n"
	match _clone_sorting:
		zellij.CLONE_SORT.DEFAULT:
			$info.text += "clone sorting: rotation\n"
		zellij.CLONE_SORT.ALIGN:
			$info.text += "clone sorting: aligned first\n"
		zellij.CLONE_SORT.PERPENDICULAR:
			$info.text += "clone sorting: perpendicular first\n"
	if !tiling.empty():
		
		if tiling_processing:
			$info.text += "tiling depth: " + str(tiling.depth_request) + " (request)\n"
			$info.text += "tiling depth: " + str(tiling.levels.size()) + " (current)\n"
			$info.text += "edge: " + str(tiling.stats.edge_index) + "/" + str(tiling.stats.edge_count) + "\n"
			$info.text += "clone index: " + str(tiling.stats.clone_index) + "\n"
			$info.text += "clones validated: " + str(tiling.stats.clone_validated) + "\n"
		else:
			$info.text += "tiling depth: " + str(tiling.depth_request) + "\n"
		$info.text += "clones tested: " + str(tiling.stats.clone_tested) + "\n"
		$info.text += "tiles: " + str(tiling.stats.tiles) + "\n"
		
		if tiling_processing:
			var curr_level:int = tiling.levels.size() - 1
			if curr_level >= $progress_bars.get_child_count():
				var pgb:Polygon2D = $progress.duplicate()
				for c in $progress_bars.get_children():
					c.color.r *= 0.5
					c.color.b *= 0.5
				$progress_bars.add_child( pgb )
				pgb.visible = true
				pgb.position = Vector2( 0, (tiling.levels.size()-1) * -5 )
				pgb.scale.y = 5
			
			var vps:Vector2 = get_viewport().size
			for i in range( 0, $progress_bars.get_child_count() ):
				var pb:Node2D = $progress_bars.get_child(i)
				if i == curr_level:
					pb.scale.x = vps.x * ( tiling.stats.edge_index * 1.0 / tiling.stats.edge_count )
				else:
					pb.scale.x = vps.x
			$progress_bars.position = Vector2( 0, vps.y )
	
	$progress_bars.visible = tiling_processing

func refresh() -> void:
	
	purge()
	
	$basis.position = get_viewport().size * 0.5
	$tiles.position = get_viewport().size * 0.5
	
	var polygon:Array = []
	
	for v in shape.vertices:
		var pos:Vector2 = v.pos * _radius
		polygon.append( pos )
	
	$basis.polygon = polygon
	
	info()

func update_tiling_display() -> void:
	
	if tiling.empty():
		return

	var polygon:Array
	
	while tiling_curr_depth < tiling.levels.size():
		var clones:Array = tiling.levels[ tiling_curr_depth ]
		var ccount:int = clones.size()
		var hslice:float = 1.0 / (_depth+1)
		var hoffset:float = hslice * tiling_curr_depth
		var hgap:float = hslice / ccount
		for i in range(0,ccount):
			var tile:Polygon2D = Polygon2D.new()
			tile.color = Color().from_hsv( hoffset + hgap * i, 1, 1 )
			polygon = []
			for v in clones[i].vertices:
				polygon.append( v.pos * _radius )
			tile.polygon = polygon
			$tiles.add_child( tile )
			var border:Line2D = Line2D.new()
			border.default_color = Color.black
			border.width = 1
			polygon.append( polygon[0] )
			border.points = polygon
			$tiles.add_child( border )
		tiling_curr_depth += 1
	
	info()

# GENERAL ######################

func _ready() -> void:
	tiler.connect("shape_validated", self, "shape_validated")
	tiler.connect("tiling_step", self, "tiling_step")
	tiler.connect("tiling_finished", self, "tiling_finished")
	generate_shape()
	$progress_bars.visible = false

func _input(event) -> void:
	if event is InputEventKey and event.pressed:
		match event.scancode:
			KEY_E:
				set_edge_sorting( _edge_sorting + 1 )
				generate_tiling()
			KEY_C:
				set_clone_sorting( _clone_sorting + 1 )
				generate_tiling()
			KEY_SPACE:
				generate_shape()
			KEY_UP:
				set_edges( _edges + 1 )
				generate_shape()
			KEY_DOWN:
				set_edges( max( 3, _edges - 1 ) )
				generate_shape()
			KEY_0:
				set_depth(0)
				generate_tiling()
			KEY_1:
				set_depth(1)
				generate_tiling()
			KEY_2:
				set_depth(2)
				generate_tiling()
			KEY_3:
				set_depth(3)
				generate_tiling()
			KEY_4:
				set_depth(4)
				generate_tiling()
			KEY_5:
				set_depth(5)
				generate_tiling()

func _process(delta) -> void:
	if tiling_processing:
		info()
