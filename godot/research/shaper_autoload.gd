extends Node2D

onready var tiler:Node = get_node("/root/zellij_tiler")

export(float,0,500) var _radius:float = 60 setget set_radius
export(int,2,100) var _edges:int = 6 setget set_edges
export(float,0,1) var _random:float = 0 setget set_random
export(float,0,5) var _random_min:float = 2
export(float,0,5) var _random_max:float = 2
export(bool) var _draw_vertex:bool = false
export(bool) var _draw_normal:bool = false
export(bool) var _edge_id:bool = false
export(int,-1,100) var _display_clone:int = -1 setget set_display_clone
export(float,0,100) var _offset_clone:float = 5 setget set_offset_clone
export(bool) var _display_invalid:bool = false setget set_display_invalid
export(bool) var _display_first:bool = false setget set_display_first
export(int,0,2) var _clone_sorting:int = 0 setget set_clone_sorting

var vertices_raw:Array = []
var shape:Dictionary = {} # main shape

func set_radius(f:float) -> void:
	_radius = f
	if is_inside_tree():
		refresh()

func set_edges(i:int) -> void:
	_edges = i

func set_random(f:float) -> void:
	_random = f

func set_display_clone(i:int) -> void:
	_display_clone = i
	if "edges" in shape.keys():
		_display_clone %= shape.edges.size()
	if is_inside_tree():
		refresh()

func set_offset_clone(f:float) -> void:
	_offset_clone = f
	if is_inside_tree():
		refresh()

func set_display_invalid(b:bool) -> void:
	_display_invalid = b
	if is_inside_tree():
		refresh()

func set_display_first(b:bool) -> void:
	_display_first = b
	if is_inside_tree():
		refresh()

func set_clone_sorting(i:int) -> void:
	_clone_sorting = i % 3
	if is_inside_tree():
		refresh()

# CREATION ######################

func generate() -> void:
	
	if not is_inside_tree():
		return
	
	# cleanup
	vertices_raw = []
	
	randomize()
	var agap:float = TAU / _edges
	for i in range(0,_edges):
		var a:float = i * agap
		var vert:Vector2 = Vector2(cos(a),sin(a))
		if rand_range(0,1) < _random:
			vert *= rand_range( _random_min, _random_max )
		vertices_raw.append( vert )
	
	# this will generates a signal
	tiler.create_shape( self, vertices_raw )

# DISPLAY ######################

func purge() -> void:
	
	while $basis.get_child_count() > 0:
		$basis.remove_child( $basis.get_child(0) )
	while $symmetric.get_child_count() > 0:
		$symmetric.remove_child( $symmetric.get_child(0) )
	while $symmetry_axis.get_child_count() > 0:
		$symmetry_axis.remove_child( $symmetry_axis.get_child(0) )
	while $clones.get_child_count() > 0:
		$clones.remove_child( $clones.get_child(0) )

func refresh() -> void:
	
	purge()
	
	position = get_viewport().size * 0.5
	
	var polygon:Array = []
	var polygon_sym:Array = []
	
	var vertl:Line2D = Line2D.new()
	vertl.width = 1
	vertl.add_point( Vector2(-3,-3) )
	vertl.add_point( Vector2(3,3) )
	vertl.add_point( Vector2(0,0) )
	vertl.add_point( Vector2(3,-3) )
	vertl.add_point( Vector2(-3,3) )
	vertl.default_color = Color.red
	
	if _draw_vertex:
		for v in vertices_raw:
			var vl:Line2D = vertl.duplicate()
			$basis.add_child( vl )
			vl.position = v * _radius
	
	vertl.clear_points()
	vertl.add_point( Vector2(-5,0) )
	vertl.add_point( Vector2(5,0) )
	vertl.add_point( Vector2(0,0) )
	vertl.add_point( Vector2(0,-5) )
	vertl.add_point( Vector2(0,5) )
	vertl.default_color = Color.green
	
	var normal:Line2D = Line2D.new()
	normal.width = 1
	normal.add_point( Vector2(0,0) )
	normal.add_point( Vector2(20,0) )
	normal.default_color = Color(1,0,1,0.7)
	
	for v in shape.vertices:
		var pos:Vector2 = v.pos * _radius
		if _draw_vertex:
			var vl:Line2D = vertl.duplicate()
			$basis.add_child( vl )
			vl.position = pos
		if _draw_normal:
			var nl:Line2D = normal.duplicate()
			$basis.add_child( nl )
			nl.position = pos
			nl.rotation = Vector2.RIGHT.angle_to( v.normal )
		polygon.append( pos )
		
	if shape.symmetric != null:
		for v in shape.symmetric.vertices:
			var pos:Vector2 = v.pos * _radius
			if _draw_vertex:
				var vl:Line2D = vertl.duplicate()
				$symmetric.add_child( vl )
				vl.position = pos
			if _draw_normal:
				var nl:Line2D = normal.duplicate()
				$symmetric.add_child( nl )
				nl.position = pos
				nl.rotation = Vector2.RIGHT.angle_to( v.normal )
			polygon_sym.append( pos )
	
	normal.clear_points()
	normal.add_point( Vector2(0,0) )
	normal.add_point( Vector2(10,0) )
	
	for e in shape.edges:
		if _draw_normal:
			var nl:Line2D = normal.duplicate()
			$basis.add_child( nl )
			nl.position = e.mid * _radius
			nl.rotation = Vector2.RIGHT.angle_to( e.normal )
		if _edge_id:
			var lbl:Label = $lbl.duplicate()
			lbl.set( "custom_colors/font_color", $basis.color )
			lbl.text = str( e.uid )
			$basis.add_child( lbl )
			lbl.rect_position = e.mid * _radius + e.normal * 20 - lbl.rect_size * 0.5
			lbl.visible = true
	if shape.symmetric != null:
		for e in shape.symmetric.edges:
			if _draw_normal:
				var nl:Line2D = normal.duplicate()
				$symmetric.add_child( nl )
				nl.position = e.mid * _radius
				nl.rotation = Vector2.RIGHT.angle_to( e.normal )
			if _edge_id:
				var lbl:Label = $lbl.duplicate()
				lbl.set( "custom_colors/font_color", $symmetric.default_color )
				lbl.text = str( e.uid )
				$symmetric.add_child( lbl )
				lbl.rect_position = e.mid * _radius + e.normal * 30 - lbl.rect_size * 0.5
				lbl.visible = true
	
	vertl.queue_free()
	normal.queue_free()
	
	$basis.polygon = polygon
	if !polygon_sym.empty():
		polygon_sym.append( polygon_sym[0] )
	$symmetric.points = polygon_sym
	
	var syml:Line2D = Line2D.new()
	syml.width = 1
	syml.default_color = Color( 1,1,0,0.8 )
	for axis in shape.symmetry_axis:
		var al:Line2D = syml.duplicate()
		al.add_point( axis * _radius * _random_max )
		al.add_point( axis * -_radius * _random_max )
		$symmetry_axis.add_child( al )
	
	var clonep:Polygon2D = Polygon2D.new()
	clonep.color = Color( 1,1,1,0.2 )
	var clonel:Line2D = Line2D.new()
	clonel.width = 1
	clonel.default_color = Color( 0.7,0,1,0.8 )
	
	for ci in range( 0, shape.clones.size() ):
		
		if _display_clone != -1 and ci != _display_clone:
			continue
		
		var edge_clones:Array = shape.clones[ci]
		var indices:Array = []
		match _clone_sorting:
			0:
				indices = shape.sorting.clone_rotation[ci]
			1:
				indices = shape.sorting.clone_alignment[ci]
			2:
				indices = shape.sorting.clone_perpendicular[ci]
		
		var clone_offset:Vector2 = Vector2.ZERO
		var display_count:int = 0
		
		for index in indices:
			if _display_first and display_count >= 1:
				break
			var clone:Dictionary = edge_clones[ index ]
			if !clone.valid:
				continue
			clone_offset += shape.edges[ci].normal * _offset_clone
			var cl:Line2D = clonel.duplicate()
			$clones.add_child( cl )
			if clone.symmetry:
				cl.default_color = Color( 0,0.75,1,0.8 )
			polygon = []
			for i in range(0,clone.vertices.size()+1):
				var vi:int = i % clone.vertices.size()
				var v:Vector2 = clone.vertices[vi].pos * _radius
				if i < clone.vertices.size():
					polygon.append( v )
				cl.add_point( v )
			cl.position = clone_offset
			display_count += 1
			
		if _display_invalid:
			for index in indices:
				if _display_first and display_count >= 1:
					break
				var clone:Dictionary = edge_clones[ index ]
				if clone.valid:
					continue
				clone_offset += shape.edges[ci].normal * _offset_clone
				var cl:Line2D = clonel.duplicate()
				cl.default_color = Color( 0.9,0,0,0.75 )
				$clones.add_child( cl )
				polygon = []
				for i in range(0,clone.vertices.size()+1):
					var vi:int = i % clone.vertices.size()
					var v:Vector2 = clone.vertices[vi].pos * _radius
					if i < clone.vertices.size():
						polygon.append( v )
					cl.add_point( v )
				cl.position = clone_offset
				display_count += 1
	
	clonep.queue_free()
	clonel.queue_free()
	
	$info.text = "radius " + str(_radius) + "\n"
	$info.text += "edges " + str(_edges) + " (request)\n"
	$info.text += "edges " + str(shape.edges.size()) + " (actual)\n"
	var clone_count:int = 0
	var valid_clone_count:int = 0
	for ec in shape.clones:
		for c in ec:
			clone_count += 1
			if c.valid:
				valid_clone_count += 1
	$info.text += "clones " + str(valid_clone_count) + " (valids)\n"
	$info.text += "clones " + str(clone_count) + " (total)\n"
	match _clone_sorting:
		0:
			$info.text += "clone sorting: rotation\n"
		1:
			$info.text += "clone sorting: aligned first\n"
		2:
			$info.text += "clone sorting: perpendicular first\n"
	$info.text += "invalid clones: " + str(_display_invalid) + "\n"
	$info.text += "only first clone: " + str(_display_first) + "\n"
	$info.rect_position = Vector2(int( shape.max.x * 1.1 * _radius),0)

func shape_created( caller:Node ) -> void:
	if self != caller:
		return
	print( "shape created, waiting for shape to be validated" )

func shape_validated( caller:Node, shape:Dictionary ) -> void:
	if self != caller:
		return
	print( "shape validated" )
	self.shape = shape
	if self.shape.valid:
		set_display_clone( _display_clone )
		refresh()
	else:
		purge()

# GENERAL ######################

func _ready() -> void:
	tiler.connect("shape_created", self, "shape_created")
	tiler.connect("shape_validated", self, "shape_validated")
	generate()

func _input(event) -> void:
	if event is InputEventKey and event.pressed:
		match event.scancode:
			KEY_I:
				set_display_invalid( !_display_invalid )
			KEY_F:
				set_display_first( !_display_first)
			KEY_S:
				set_clone_sorting( _clone_sorting + 1 )
			KEY_SPACE:
				generate()
			KEY_UP:
				set_edges( _edges + 1 )
				generate()
			KEY_DOWN:
				set_edges( max( 3, _edges - 1 ) )
				generate()
			KEY_RIGHT:
				set_display_clone( _display_clone + 1 )
			KEY_LEFT:
				set_display_clone( max( -1, _display_clone - 1 ) )
