# zellij

Pattern generator based on the mathematical construction of islamic patterns described in the book *L'art des motifs islamiques*, Eric Broug, ISBN 978-2-35017-299-6

## repository structure

- `assets`: images, fonts and icons used in the project
- `godot`: standalone project to be run with Godot (3.2 and above) with UI and export function

## greetings

## notes

- aperiodic geometry https://www.youtube.com/watch?v=eKjZ2W7eXlo
- Heesch Numbers and Tiling (numberphile) https://www.youtube.com/watch?v=6aFcgATW9Mw

## credits

Zellij is under GPL v.3 license. Modify it at will, share, but don't forget to share your modifications!

A [polymorph.cool](https:/polymorph.cool) object.
